//
//  String+Ext.swift
//  TODO
//
//  Created by Marcin Jucha on 07.06.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

extension String {
  func toColor() -> GroupColor {
    switch self {
    case "RED":
      return .red
    case "ORANGE":
      return .oragne
    case "YELLOW":
      return .yellow
    case "GREEN":
      return .green
    case "TEAL_BLUE":
      return .tealBlue
    case "BLUE":
      return .blue
    case "PURPLE":
      return .purple
    case "PINK":
      return .pink
    default:
      return .red
    }
  }
}
