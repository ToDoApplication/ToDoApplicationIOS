//
//  UIViewController+Ext.swift
//  TODO
//
//  Created by Marcin Jucha on 27.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

extension UIViewController {
  func presentTodoList(_ token: String) {
    if let vc = UIStoryboard.createMainNav() {
      present(vc, animated: true, completion: nil)
    }
  }
}
