//
//  Todo+Ext.swift
//  TODO
//
//  Created by Marcin Jucha on 01.06.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

extension Todo {
  func copy() -> Todo {
    let todo = Todo()
    
    todo.id = id
    todo.favorite = favorite
    todo.title = title
    todo.type = type
    todo.text = text
    todo.tmpId = tmpId
    
    for item in list {
      todo.addTodoListItem(item.copy())
    }
    
    return todo
  }
}

extension TodoListItem {
  func copy() -> TodoListItem {
    let item = TodoListItem()
    item.id = id
    item.text = text
    item.completed = completed
    item.tmpId = tmpId
    
    return item
  }
}


extension TodoGroup: Equatable {
  static func toTodoGroup(_ groups: [Response.TodoGroup]) -> [TodoGroup] {
    var groupList = [TodoGroup]()
    for val in groups {
      let group = TodoGroup(id: val.id, title: val.name, color: val.colour?.toColor(), list: Todo.toTodo(val.tasks))
      groupList.append(group)
    }
    return groupList
  }
  
  static func toSingleTodoGroup(_ group: Response.TodoGroup) -> TodoGroup {
    return TodoGroup(id: group.id, title: group.name, color: group.colour?.toColor(), list: Todo.toTodo(group.tasks))
  }
  
  func toStruct() -> Response.TodoGroup {
    return Response.TodoGroup(id: id, name: title, colour: color.toString(), tasks: [])
  }
}

func ==(lhs: TodoGroup, rhs: TodoGroup) -> Bool {
  return lhs.id == rhs.id
    && lhs.title == rhs.title
    && lhs.list == rhs.list
    && lhs.tmpId == rhs.tmpId
}

extension Todo: Equatable {
  static func toTodo(_ todoList: [Response.Task]?) -> [Todo] {
    var list = [Todo]()
    for todo in (todoList ?? []) {
      let item = Todo(id: todo.id, title: todo.name, favorite: todo.favored, text: todo.text, list: TodoListItem.toTodoItem(todo.list))
      list.append(item)
    }
    return list
  }
  
  func toStruct(_ groupId: Int?) -> Response.Task {
    var todos: [Response.Todo] = []
    for todo in list {
      todos.append(todo.toStruct())
    }
    return Response.Task(groupId: groupId, id: id, name: title, text: text, favored: favorite, list: todos, type: type)
  }
}

func ==(lhs: Todo, rhs: Todo) -> Bool {
  return lhs.id == rhs.id
    && lhs.title == rhs.title
    && lhs.favorite == rhs.favorite
    && lhs.type == rhs.type
    && lhs.text == rhs.text
    && lhs.list == rhs.list
    && lhs.tmpId == rhs.tmpId
}

extension TodoListItem: Equatable {
  static func toTodoItem(_ itemList: [Response.Todo]?) -> [TodoListItem] {
    var items = [TodoListItem]()
    for val in (itemList ?? []) {
      let item = TodoListItem(id: val.id, text: val.text, completed: val.completed)
      items.append(item)
    }
    return items
  }
  
  func toStruct() -> Response.Todo {
    return Response.Todo(id: id, text: text, completed: completed)
  }
}

func ==(lhs: TodoListItem, rhs: TodoListItem) -> Bool {
  return lhs.id == rhs.id
    && lhs.completed == rhs.completed
    && lhs.text == rhs.text
    && lhs.tmpId == rhs.tmpId
}
