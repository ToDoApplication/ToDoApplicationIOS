//
//  NSNotification.Name+Ext.swift
//  TODO
//
//  Created by Marcin Jucha on 19.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

extension Notification.Name {
  public static let MENU_NOTIFICATION = Notification.Name(rawValue: "menu-notification")
  public static let MENU_EDIT_NOTIFICATION = Notification.Name(rawValue: "menu-edit-notification")
}
