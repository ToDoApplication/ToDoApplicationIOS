//
//  Array+Ext.swift
//  TODO
//
//  Created by Marcin Jucha on 22.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

extension MutableCollection  {
  func indexOfElement<T: Equatable>(element: T) -> Int? where T == Iterator.Element {
    for (i, e) in self.enumerated() {
      if e == element {
        return i
      }
    }
    return nil
  }
}
