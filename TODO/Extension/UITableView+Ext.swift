//
//  UITableView+Ext.swift
//  TODO
//
//  Created by Marcin Jucha on 08.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

extension UITableView {
  
  func register<T: UITableViewCell>(_: T.Type) {
    let nib = UINib(nibName: T.nibName, bundle: Bundle.main)
    self.register(nib, forCellReuseIdentifier: T.reuseIdentifier)
//    self.register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
  }
  
  func dequeueReusableCell<T: UITableViewCell>(forIndex index: IndexPath) -> T  {
    guard let cell = self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: index) as? T else {
      fatalError("Podany cell z id \(T.reuseIdentifier) nie istnieje")
    }
    return cell
  }
  
  func setProperties(_ controller: UITableViewDelegate & UITableViewDataSource) {
    self.dataSource = controller
    self.delegate = controller
    self.estimatedRowHeight = 70
    self.rowHeight = UITableViewAutomaticDimension
  }
}
