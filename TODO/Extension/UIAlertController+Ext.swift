//
//  UIAlertController+Ext.swift
//  TODO
//
//  Created by Marcin Jucha on 18.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

extension UIAlertController {
  static func createAlertWithTextField(title: String?, text: String, saveHandler: @escaping (_ text: String) -> ()) -> UIAlertController {
    let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
    
    let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
      alert in
      
      let textField = alertController.textFields![0]
      
      saveHandler(textField.text ?? "")
    })
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
      action in
      
    })
    
    alertController.addTextField { (textField : UITextField!) -> Void in
      textField.placeholder = "Enter Your Text"
      if text != "" {
        textField.text = text
      }
    }
    
    alertController.addAction(saveAction)
    alertController.addAction(cancelAction)

    return alertController
  }
  
  static func deleteAlertWithText(title: String?, message: String? = nil, handler: @escaping () -> ()) -> UIAlertController {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let saveAction = UIAlertAction(title: "Delete", style: .default, handler: {
      alert in
      
      handler()
    })
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in})
    
    alertController.addAction(saveAction)
    alertController.addAction(cancelAction)
    
    return alertController
  }
  
  static func createAlertWithText(title: String?, message: String? = nil, handler: (() -> ())? = nil) -> UIAlertController {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let saveAction = UIAlertAction(title: "OK", style: .default, handler: {
      alert in
      if let handler = handler {
        handler()
      }
    })
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in})
    
    alertController.addAction(saveAction)
    alertController.addAction(cancelAction)
    
    return alertController
  }
  
  static func createOneButtonAlertWithText(title: String?, message: String? = nil, handler: (() -> ())? = nil) -> UIAlertController {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let saveAction = UIAlertAction(title: "OK", style: .default, handler: {
      alert in
      if let handler = handler {
        handler()
      }
    })
    
    
    alertController.addAction(saveAction)
    
    return alertController
  }
}
