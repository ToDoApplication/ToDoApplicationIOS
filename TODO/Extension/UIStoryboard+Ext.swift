//
//  UIStoryboard+Ext.swift
//  TODO
//
//  Created by Marcin Jucha on 21.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation


extension UIStoryboard {
  private static var main: String {
    return "Main"
  }
  
  private static var menu: String {
    return "Menu"
  }
  
  private static var todoList: String {
    return "TodoList"
  }
  
  private static func createViewController<T: UIViewController>(storyboardId: String, viewControllerId: String) -> T? {
    let sb = UIStoryboard(name: storyboardId, bundle: Bundle.main)
    let vc = sb.instantiateViewController(withIdentifier: viewControllerId) as? T
    return vc
  }
  
  static func createTodoGroupVC(delegate: TodoGroupVCDelegate? = nil, group: TodoGroup? = nil) -> TodoGroupVC? {
    let vc = createViewController(storyboardId: menu, viewControllerId: TODO_GROUP_VC_IDENTIFIER) as? TodoGroupVC
    vc?.todoGroup = group
    vc?.delegate = delegate
    return vc
  }
  
  static func createMainNav() -> UIViewController? {
    let vc = createViewController(storyboardId: main, viewControllerId: MAIN_NAV_IDENTIFIER)
    return vc
  }
}
