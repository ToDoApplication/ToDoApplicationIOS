//
//  UIScrollView+Ext.swift
//  TODO
//
//  Created by Marcin Jucha on 18.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

extension UIScrollView {
  
  /// listen to keyboard event
  func makeAwareOfKeyboard() {
    NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
  }
  
  /// move scroll view up
  func keyBoardDidShow(notification: Notification) {
//    let info = notification.userInfo as NSDictionary?
//    let rectValue = info![UIKeyboardFrameBeginUserInfoKey] as! NSValue
//    let kbSize = rectValue.cgRectValue.size
    
//    let contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0)
    UIView.animate(withDuration: 0.2) { 
      self.contentOffset.y = 10.0
    }
//    self.scrollIndicatorInsets = contentInsets
    
  }
  
  /// move scrollview back down
  func keyBoardDidHide(notification: Notification) {
    // restore content inset to 0
//    let contentInsets = UIEdgeInsetsMake(0, 0, 0, 0)
//    self.contentInset = contentInsets
//    self.scrollIndicatorInsets = contentInsets
    
    UIView.animate(withDuration: 0.2) { 
      self.contentOffset.y = 0.0
    }
  }
}
