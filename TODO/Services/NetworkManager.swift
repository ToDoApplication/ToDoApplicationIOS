//
//  NetworkManager.swift
//  TODO
//
//  Created by Marcin Jucha on 05.06.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation
import Alamofire
import Gloss

fileprivate let URL_BASE = "https://localhost:8443"
//fileprivate let URL_BASE = "http://localhost:8080"
fileprivate let URL_LOGIN = "/api/login"
fileprivate let URL_TODO_GROUP = "/api/grouped"
fileprivate let URL_TASK = "/api/task"
fileprivate let URL_TODO_GROUP_BY_ID = "/api/grouped/group?id="

enum HttpStatusMessage: Int {
  case unauthorized = 401
  case internalServerError = 500
  
  func message() -> String {
    switch self {
    case .unauthorized:
      return "Nieautoryzowany dostęp - wymagane jest logowanie."
    case .internalServerError:
      return "Wystąpił nieoczekiwany błąd serwera."
    }
  }
}

protocol NetworkManagerProtocol {
  var headers: HTTPHeaders? { get }
  
  var manager: SessionManager { get }
  
  var delegate: NetworkManagerDelegate? { get set }
  
  func signInUser(username: String, token: String, completion: @escaping (Error?) -> ())
  
  // ****************************************
  // Todo Group
  // ****************************************
  func getTodoGroupList(completion: @escaping ([Response.TodoGroup]) -> ())
  
  func addTodoGroup(_ group: Response.TodoGroup, completion: @escaping (Response.TodoGroup) -> ())
  
  func updateTodoGroup(_ group: Response.TodoGroup)
  
  func deleteTodoGroup(_ group: Response.TodoGroup)
  
  // ****************************************
  // Todo
  // ****************************************
  func addTodo(_ task: Response.Task, completion: @escaping (Response.TodoGroup) -> ())
  
  func updateTodo(_ task: Response.Task, completion: @escaping (Response.TodoGroup) -> ())
  
  func deleteTodo(_ task: Response.Task)
}

extension NetworkManagerProtocol {
  var headers: HTTPHeaders? {
    return nil
  }
  
  var manager: SessionManager {
    return Alamofire.SessionManager.default
  }
  
  func deleteRequest(_ request: String, params: JSON?, completion: @escaping (DataResponse<Any>, Error?) -> ()) {
    callRequest(method: .delete, request: request, params: params) {
      response, error in
      completion(response, error)
    }
  }
  
  func putRequest(_ request: String, params: JSON?, completion: @escaping (DataResponse<Any>, Error?) -> ()) {
    callRequest(method: .put, request: request, params: params) {
      response, error in
      completion(response, error)
    }
  }
  
  func postRequest(_ request: String, params: JSON?, completion: @escaping (DataResponse<Any>, Error?) -> ()) {
    callRequest(method: .post, request: request, params: params) {
      response, error in
      completion(response, error)
    }
  }
  
  func getRequest(_ request: String, params: JSON?, completion: @escaping (DataResponse<Any>, Error?) -> ()) {
    callRequest(method: .get, request: request, params: params) {
      response, error in
      completion(response, error)
    }
  }
  
  private func callRequest(method: HTTPMethod, request: String, params: JSON?, completion: @escaping (DataResponse<Any>, Error?) -> ()) {
    manager.request("\(URL_BASE)\(request)",
        method: method,
        parameters: params,
        encoding: JSONEncoding.default,
        headers: headers).responseJSON { response in
          
          if let code = response.response?.statusCode, code == 200 {
            completion(response, nil)
          } else if let error = response.error {
            completion(response, response.error)
            if let httpStatus = response.response?.statusCode,
              let message = HttpStatusMessage(rawValue: httpStatus) {
              self.delegate?.displayAlert(message)
            } else {
              self.delegate?.displayAlert(error.localizedDescription)
            }
          }
        }
  }
}

class NetworkManager: NetworkManagerProtocol {
  var delegate: NetworkManagerDelegate?

  private var _header: String?
  
  var headers: HTTPHeaders? {
    if let header = _header {
      return ["Authorization" : header]
    }
    return nil
  }
  
  private var _manager: SessionManager = {
    let sessionManager = SessionManager.default
    sessionManager.delegate.sessionDidReceiveChallenge = { session, challenge in
      var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
      var credential: URLCredential?
      
      if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust, let trust = challenge.protectionSpace.serverTrust {
        disposition = URLSession.AuthChallengeDisposition.useCredential
        credential = URLCredential(trust: trust)
      } else {
        if challenge.previousFailureCount > 0 {
          disposition = .cancelAuthenticationChallenge
        } else {
          credential = sessionManager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
          
          if credential != nil {
            disposition = .useCredential
          }
        }
      }
      
      return (disposition, credential)
    }

    return sessionManager
  }()
  
  var manager: SessionManager {
    return _manager
  }
  
  func signInUser(username: String, token: String, completion: @escaping (Error?) -> ()) {
    print("Username - " + username)
    print("Token - " + token)
    postRequest(URL_LOGIN, params: ["username": username, "token": token]) { (response, error) in
      if let header = response.response?.allHeaderFields["Authorization"] as? String {
        self._header = header.substring(from: header.index(header.startIndex, offsetBy: 7))
      }
      print("\nHeader - " + (self._header ?? ""))
      completion(error)
    }
  }
}

// ****************************************
// MARK: Todo
// ****************************************
extension NetworkManager {
  private func getJson(_ task: Response.Task) -> JSON {
    var json = task.toJSON() ?? [:]
    var todoArray = [JSON]()
    for val in (task.list ?? []) {
      todoArray.append(val.toJSON() ?? [:])
    }
    json["list"] = todoArray
    return json
  }
  
  func addTodo(_ task: Response.Task, completion: @escaping (Response.TodoGroup) -> ()) {
    postRequest(URL_TASK, params: getJson(task)) { (response, error) in
      self.getRequest("\(URL_TODO_GROUP_BY_ID)\(task.groupId!)", params: nil, completion: { (res, err) in
        if let json = res.result.value as? JSON,
          let group = Response.TodoGroup(json: json) {
          completion(group)
        }
      })
    }
  }
  
  func updateTodo(_ task: Response.Task, completion: @escaping (Response.TodoGroup) -> ()) {
    putRequest(URL_TASK, params: getJson(task)) { (res, err) in
      self.getRequest("\(URL_TODO_GROUP_BY_ID)\(task.groupId!)", params: nil, completion: { (res, err) in
        if let json = res.result.value as? JSON,
          let group = Response.TodoGroup(json: json) {
          completion(group)
        }
      })
    }
  }
  
  func deleteTodo(_ task: Response.Task) {
    deleteRequest(URL_TASK, params: getJson(task)) { (res, err) in
      if let error = err {
        print(error.localizedDescription + String(describing: res.response?.statusCode))
      }
    }
  }
}

// ****************************************
// MARK: Todo Group
// ****************************************
extension NetworkManager {
  func getTodoGroupList(completion: @escaping ([Response.TodoGroup]) -> ()) {
    getRequest(URL_TODO_GROUP, params: nil) {
      response, error in
      var groups: [Response.TodoGroup] = []
      if let json = response.value as? [JSON] {
        for group in json {
          if let val = Response.TodoGroup(json: group) {
            groups.append(val)
          }
        }
      }
      completion(groups)
    }
  }
  
  func addTodoGroup(_ group: Response.TodoGroup, completion: @escaping (Response.TodoGroup) -> ()) {
    postRequest(URL_TODO_GROUP, params: group.toJSON()) { (response, error) in
      if let json = response.value as? JSON,
        let group = Response.TodoGroup(json: json) {
        completion(group)
      }
    }
  }
  
  func updateTodoGroup(_ group: Response.TodoGroup) {
    putRequest(URL_TODO_GROUP, params: group.toJSON()) { (response, error) in
      if let error = error {
        print(error.localizedDescription + String(describing: response.response?.statusCode))
      }
    }
  }
  
  func deleteTodoGroup(_ group: Response.TodoGroup) {
    deleteRequest(URL_TODO_GROUP, params: group.toJSON()) { (response, error) in
      if let error = error {
        print(error.localizedDescription + String(describing: response.response?.statusCode))
      }
    }
  }
}



