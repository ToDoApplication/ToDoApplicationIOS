//
//  TodoGroupManager.swift
//  TODO
//
//  Created by Marcin Jucha on 19.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol TodoGroupManagerProtocol {
  func getTodoGroupList() -> [TodoGroup]
  func addTodoGroup(_ todoGroup: TodoGroup, completion: @escaping () -> ())
  func updateTodoGroup(_ todoGroup: TodoGroup, completion: @escaping () -> ())
  func deleteTodoGroup(_ todoGroup: TodoGroup)
  
  func loadTodoGroupList(completion: @escaping () -> ())
}

class TodoGroupManager: TodoGroupManagerProtocol {
  private var todoList = [TodoGroup]()
  
  func loadTodoGroupList(completion: @escaping () -> ()) {
    networkManager.getTodoGroupList() {
      groups in
        self.todoList = TodoGroup.toTodoGroup(groups)
        completion()
    }
  }
  
  func getTodoGroupList() -> [TodoGroup] {
    return todoList
  }
  
  func addTodoGroup(_ todoGroup: TodoGroup, completion: @escaping () -> ()) {
    todoList.append(todoGroup)
    networkManager.addTodoGroup(todoGroup.toStruct()) { (group) in
      todoGroup.id = group.id ?? -1
      completion()
    }
  }
  
  func updateTodoGroup(_ todoGroup: TodoGroup, completion: @escaping () -> ()) {
    networkManager.updateTodoGroup(todoGroup.toStruct())
    completion()
  }
  
  func deleteTodoGroup(_ todoGroup: TodoGroup) {
    for (i, v) in todoList.enumerated() {
      if v.id == todoGroup.id {
        todoList.remove(at: i)
        break
      }
    }
    networkManager.deleteTodoGroup(todoGroup.toStruct())
  }
}
