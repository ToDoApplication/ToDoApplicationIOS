//
//  Constants.swift
//  TODO
//
//  Created by Marcin Jucha on 10.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

let TODO_DETAILS_SEGUE = "todoDetailsVCSegue"
let MAIN_SEGUE = "mainSegue"

let MAIN_NAV_IDENTIFIER = "MainNav"
let TODO_VC_IDENTIFIER = "TodoVC"
let TODO_DETAILS_VC_IDENTIFIER = "TodoDetailsVC"

let MENU_VC_IDENTIFIER = "MenuVC"
let TODO_GROUP_VC_IDENTIFIER = "TodoGroupVC"

let UNWIND_TO_TODO_LIST = "unwindToTodoList"
let UNWIND_TO_LOGIN = "unwindToLogin"
let UNWIND_TO_LOGIN_LIST = "unwindToLoginTodo"

