//
//  WSResponse.swift
//  TODO
//
//  Created by Marcin Jucha on 06.06.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation
import Gloss

struct Response {
  struct TodoGroup: Glossy {
    let id: Int?
    let name: String?
    let colour: String?
    let tasks: [Task]?
    
    init(id: Int?, name: String?, colour: String?, tasks: [Task]?) {
      self.id = id
      self.name = name
      self.colour = colour
      self.tasks = tasks
    }
    
    init?(json: JSON) {
      self.id = "id" <~~ json
      self.name = "name" <~~ json
      self.colour = "colour" <~~ json
      self.tasks = "tasks" <~~ json
    }
    
    func toJSON() -> JSON? {
      return jsonify([
        "id" ~~> self.id,
        "name" ~~> self.name,
        "colour" ~~> self.colour,
        "tasks" ~~> self.tasks
        ])
    }
  }
  
  struct Task: Glossy {
    let groupId: Int?
    let id: Int?
    let name: String?
    let text: String?
    let favored: Bool?
    let list: [Todo]?
    let type: TodoType?
    
    init(groupId: Int?, id: Int?, name: String?, text: String?, favored: Bool?, list: [Todo]?, type: TodoType?) {
      self.groupId = groupId
      self.id = id
      self.name = name
      self.text = text
      self.favored = favored
      self.list = list
      self.type = type
    }
    
    init?(json: JSON) {
      self.groupId = nil
      self.id = "id" <~~ json
      self.name = "name" <~~ json
      self.text = "text" <~~ json
      self.favored = "favored" <~~ json
      self.list = "list" <~~ json
      
      if let val = text, val.isEmpty != true {
        self.type = TodoType.plain
      } else {
        self.type = TodoType.list
      }
    }
    
    func toJSON() -> JSON? {
      return jsonify([
        "groupId" ~~> self.groupId,
        "id" ~~> self.id,
        "name" ~~> self.name,
        "text" ~~> self.text,
        "favored" ~~> self.favored
        ])
    }

  }
  
  struct Todo: Glossy {
    let id: Int?
    let text: String?
    let completed: Bool?
    
    init(id: Int?, text: String?, completed: Bool?) {
      self.id = id
      self.text = text
      self.completed = completed
    }
    
    init?(json: JSON) {
      self.id = "id" <~~ json
      self.text = "text" <~~ json
      self.completed = "completed" <~~ json
    }
    
    func toJSON() -> JSON? {
      return jsonify([
        "id" ~~> self.id,
        "text" ~~> self.text,
        "completed" ~~> self.completed
        ])
    }
  }
}
