//
//  TodoItemList.swift
//  TODO
//
//  Created by Marcin Jucha on 28.08.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

fileprivate var tmpIdList = -1

class TodoListItem {
  fileprivate var _id: Int?
  var tmpId: Int = {
    tmpIdList -= 1
    return tmpIdList
  }()
  private var _completed = false
  private var _text = ""
  
  var id: Int {
    get {
      return _id ?? -1
    }
    set {
      _id = newValue
    }
  }
  
  var completed: Bool {
    get {
      return _completed
    }
    set {
      _completed = newValue
    }
  }
  
  var text: String {
    get {
      return _text
    }
    set {
      _text = newValue
    }
  }
  
  
  func getImage() -> UIImage {
    return _completed ? UIImage(named: "checkbox-tick")! : UIImage(named: "checkbox")!
  }
  
  init() {}
  
  init(id: Int?, text: String?, completed: Bool?) {
    _id = id ?? 0
    _text = text ?? ""
    _completed = completed ?? false
  }
}
