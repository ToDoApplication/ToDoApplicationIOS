//
//  Todo.swift
//  TODO
//
//  Created by Marcin Jucha on 09.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

fileprivate var tmpIdTodo = -1

class Todo {
  fileprivate var _id: Int?
  
  var tmpId: Int = {
    tmpIdTodo -= 1
    return tmpIdTodo
  }()
  var title = ""
  var favorite = false
  
  var id: Int {
    get {
      return _id ?? -1
    }
    set {
      _id = newValue
    }
  }
  
  func getImage() -> UIImage {
    return favorite ? UIImage(named: "gold-star")! : UIImage(named: "blank-star")!
  }
  
  // ****************************************
  // MARK: Details
  // ****************************************
  var type: TodoType = .list
  var text: String = ""
  fileprivate var _list: [TodoListItem] = []
  
  var list: [TodoListItem] {
    return _list
  }
  
  func addTodoListItem(_ todo: TodoListItem) {
    _list.append(todo)
  }
  
  func removeTodoListItem(_ todo: TodoListItem) {
    if let index = _list.indexOfElement(element: todo) {
      _list.remove(at: index)
    }
  }
  
  func cleanData() {
    switch type {
    case .list:
      text = ""
      let emptyValues = _list.filter { $0.text.isEmpty }
      for val in emptyValues {
        if let index = _list.indexOfElement(element: val) {
          _list.remove(at: index)
        }
      }
    case .plain:
      _list.removeAll()
    }
  }
  
  // ****************************************
  // MARK: Initializers
  // ****************************************
  convenience init(list: [TodoListItem] = []) {
    self.init()
    
    _list = list
  }
  
  init() {}
  
  init(id: Int?, title: String?, favorite: Bool?, text: String?, list: [TodoListItem] = []) {
    _id = id
    self.title = title ?? ""
    self.favorite = favorite ?? false
    if let text = text {
      type = .plain
      self.text = text
    }
    _list = list
  }
}
