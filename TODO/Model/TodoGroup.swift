//
//  TodoGroup.swift
//  TODO
//
//  Created by Marcin Jucha on 28.08.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

fileprivate var tmpIdGroup = -1

class TodoGroup {
  var delegate: TodoVCDelegte?
  
  fileprivate var _id: Int?
  
  var tmpId: Int = {
    tmpIdGroup -= 1
    return tmpIdGroup
  }()
  var title = ""
  var color = GroupColor.red
  fileprivate var _list: [Todo] = []
  
  var id: Int {
    get {
      return _id ?? -1
    }
    set {
      _id = newValue
    }
  }
  
  var list: [Todo] {
    return _list
  }
  
  private func networkManagerDelegate() {
    if let del = delegate as? NetworkManagerDelegate {
      setNetworkManagerDelegate(del)
    }
  }
  
  func addTodo(_ todo: Todo) {
    todo.cleanData()
    
    networkManagerDelegate()
    networkManager.addTodo(todo.toStruct(id)) { (group) in
      self._list = Todo.toTodo(group.tasks)
      self.delegate?.reloadData()
    }
  }
  
  func removeTodo(_ todo: Todo) {
    if let index = _list.indexOfElement(element: todo) {
      _list.remove(at: index)
    }
    
    networkManagerDelegate()
    networkManager.deleteTodo(todo.toStruct(nil))
  }
  
  func updateTodo(_ todo: Todo) {
    todo.cleanData()
    
    networkManagerDelegate()
    networkManager.updateTodo(todo.toStruct(id)) { (group) in
      self._list = Todo.toTodo(group.tasks)
      self.delegate?.reloadData()
    }
  }
  
  convenience init(list: [Todo] = []) {
    self.init()
    _list = list
  }
  
  init() {}
  
  init(id: Int?, title: String?, color: GroupColor?, list: [Todo] = []) {
    _id = id
    self.title = title ?? ""
    self.color = color ?? .blue
    _list = list
  }
  
}
