//
//  TodoEnums.swift
//  TODO
//
//  Created by Marcin Jucha on 28.08.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

enum TodoType: Int {
  case plain
  case list
}

enum GroupColor {
  case red
  case oragne
  case yellow
  case green
  case tealBlue
  case blue
  case purple
  case pink
  
  
  func color() -> UIColor {
    switch self {
    case .red:
      return UIColor.red
    case .oragne:
      return UIColor.orange
    case .yellow:
      return UIColor.yellow
    case .green:
      return UIColor.green
    case .tealBlue:
      return UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 1.0)
    case .blue:
      return UIColor.blue
    case .purple:
      return UIColor.purple
    case .pink:
      return UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1.0)
    }
  }
  
  func toString() -> String {
    switch self {
    case .red:
      return "RED"
    case .oragne:
      return "ORANGE"
    case .yellow:
      return "YELLOW"
    case .green:
      return "GREEN"
    case .tealBlue:
      return "TEAL_BLUE"
    case .blue:
      return "BLUE"
    case .purple:
      return "PURPLE"
    case .pink:
      return "PINK"
    }
  }
}
