//
//  TodoCell.swift
//  TODO
//
//  Created by Marcin Jucha on 08.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class TodoCell: UITableViewCell {

  @IBOutlet weak var titleLbl: UILabel!
  @IBOutlet weak var favoriteView: UIView!
  @IBOutlet weak var favoriteImage: UIImageView!
  
  private var todoItem: Todo!
  private var favoriteTapHandler: (() -> ())!
  
  func configureCell(_ todo: Todo, tapImageHandler: @escaping () -> ()) {
    todoItem = todo
    favoriteTapHandler = tapImageHandler
    
    titleLbl.text = todo.title
    favoriteImage.image = todo.getImage()
  }
  
  func imageHandler() {
    todoItem.favorite = !todoItem.favorite
    favoriteTapHandler()
  }
}
