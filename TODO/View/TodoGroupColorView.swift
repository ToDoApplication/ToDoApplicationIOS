//
//  TodoGroupColorView.swift
//  TODO
//
//  Created by Marcin Jucha on 22.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

enum ColorViewTag: Int {
  case red = 10
  case oragne
  case yellow
  case green
  case tealBlue
  case blue
  case purple
  case pink
  
  func color() -> GroupColor {
    switch self {
    case .red:
      return GroupColor.red
    case .oragne:
      return GroupColor.oragne
    case .yellow:
      return GroupColor.yellow
    case .green:
      return GroupColor.green
    case .tealBlue:
      return GroupColor.tealBlue
    case .blue:
      return GroupColor.blue
    case .purple:
      return GroupColor.purple
    case .pink:
      return GroupColor.pink
    }
  }
  
  static func getColorViewTag(_ color: GroupColor) -> ColorViewTag {
    switch color {
      case .red:
        return ColorViewTag.red
      case .oragne:
        return ColorViewTag.oragne
      case .yellow:
        return ColorViewTag.yellow
      case .green:
        return ColorViewTag.green
      case .tealBlue:
        return ColorViewTag.tealBlue
      case .blue:
        return ColorViewTag.blue
      case .purple:
        return ColorViewTag.purple
      case .pink:
        return ColorViewTag.pink
    }
  }
}

class TodoGroupColorView: CircleView {
  
  var selected = false {
    didSet {
      setNeedsDisplay()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    guard self.gestureRecognizers?.count != 1 else {
      return
    }
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectColor))
    self.addGestureRecognizer(tap)
  }
  
  func selectColor() {
    if let superview = superview as? TodoGroupColorStackView {
      superview.selectedColor(ColorViewTag(rawValue: tag)!)
    }
  }
  
  override func draw(_ rect: CGRect) {
    if let color = ColorViewTag(rawValue: tag) {
      circleColor = color.color().color()
    }
    
    if selected {
      let path = UIBezierPath(ovalIn: rect.insetBy(dx: 1.0, dy: 1.0))
      UIColor.black.setStroke()
      path.lineWidth = 2.0
      path.stroke()
    }
    
    super.draw(selected ? rect.insetBy(dx: 4.0, dy: 4.0) : rect)
  }
  
  func getColor() -> GroupColor {
    return ColorViewTag(rawValue: tag)!.color()
  }
}
