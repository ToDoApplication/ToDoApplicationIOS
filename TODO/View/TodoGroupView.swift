//
//  TodoGroupView.swift
//  TODO
//
//  Created by Marcin Jucha on 21.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

@IBDesignable class TodoGroupView: UIView {

  @IBInspectable var radius: CGFloat = 10.0
  @IBInspectable var color: UIColor = UIColor.red
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    let path = UIBezierPath(roundedRect: rect, cornerRadius: radius)
    
    color.setFill()
    path.fill()
  }
  
}
