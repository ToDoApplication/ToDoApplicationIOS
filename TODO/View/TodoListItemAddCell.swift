//
//  TodoAddCell.swift
//  TODO
//
//  Created by Marcin Jucha on 17.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class TodoListItemAddCell: UITableViewCell {

  @IBOutlet weak var stackView: UIStackView!
  private(set) var insertHandler: (() -> ())?
  
  override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
  }

  func configureCell(handler: @escaping () -> ()) {
    insertHandler = handler
    
    guard stackView.gestureRecognizers?.count != 1 else {
      return
    }
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapHandler))
    stackView.addGestureRecognizer(tap)
  }
  
  func tapHandler() {
    insertHandler?()
  }
}
