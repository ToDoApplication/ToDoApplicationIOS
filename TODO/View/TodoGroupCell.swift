//
//  TodoGroupCell.swift
//  TODO
//
//  Created by Marcin Jucha on 19.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class TodoGroupCell: UITableViewCell {

  @IBOutlet weak var circleView: CircleView!
  @IBOutlet weak var groupTitleLbl: UILabel!
  @IBOutlet weak var elementNumberLbl: UILabel!
  
  @IBOutlet weak var editingStackView: UIStackView!
  
  var isEditingMode = false {
    didSet {
      elementNumberLbl.isHidden = isEditingMode
      editingStackView.isHidden = !isEditingMode
    }
  }
  
  var deleteHandler: (() -> ())!
  var editHandler: (() -> ())!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  func configureCell(_ group: TodoGroup) {
    circleView.circleColor = group.color.color()
    circleView.setNeedsDisplay()
    
    groupTitleLbl.text = group.title
    elementNumberLbl.text = "\(group.list.count)"
  }
  
  @IBAction func deleteAction(_ sender: Any) {
    deleteHandler()
  }
  
  @IBAction func editAction(_ sender: Any) {
    editHandler()
  }
}
