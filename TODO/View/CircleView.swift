//
//  CircleView.swift
//  TODO
//
//  Created by Marcin Jucha on 19.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class CircleView: UIView {
  
  var circleColor = UIColor.red
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    let path = UIBezierPath(ovalIn: rect)
//    path.addClip()
    
    circleColor.setFill()
    path.fill()
    
  }
}

