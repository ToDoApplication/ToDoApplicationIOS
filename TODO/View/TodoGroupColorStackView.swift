//
//  TodoGroupColorStackView.swift
//  TODO
//
//  Created by Marcin Jucha on 22.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class TodoGroupColorStackView: UIStackView {

  override func awakeFromNib() {
    super.awakeFromNib()
    
    if let view = subviews.first as? TodoGroupColorView {
      view.selectColor()
    }
  }
  
  func getSelectedColor() -> GroupColor {
    return (subviews as! [TodoGroupColorView]).filter({ (view) -> Bool in
      return view.selected
    }).first!.getColor()
  }
  
  func selectedColor(_ color: ColorViewTag) {
    
    for subview in subviews as! [TodoGroupColorView] {
      if subview.tag == color.rawValue {
        subview.selected = true
      } else {
        subview.selected = false
      }
    }
    
  }

}
