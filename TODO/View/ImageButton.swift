//
//  ImageButton.swift
//  TODO
//
//  Created by Marcin Jucha on 16.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class ImageButton: UIButton {

  override func awakeFromNib() {
    super.awakeFromNib()
    
    imageView?.contentMode = .scaleAspectFit
  }

}
