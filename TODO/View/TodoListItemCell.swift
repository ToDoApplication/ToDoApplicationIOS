//
//  TodoCell.swift
//  TODO
//
//  Created by Marcin Jucha on 16.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class TodoListItemCell: UITableViewCell {

  @IBOutlet weak var checkboxBtn: ImageButton!
  @IBOutlet weak var textLbl: UILabel!
  
  private(set) var addTextHandler: (() -> ()) = {}
  var checkboxHandler: (() -> ()) = {}
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }

  func configureCell(_ item: TodoListItem, handler: @escaping () -> ()) {
    checkboxBtn.setImage(item.getImage(), for: .normal)
    textLbl.text = item.text
    
    addTextHandler = handler
    setTextLblTapGestureRecognizer()
    
  }
  
  @IBAction func changeCheckboxImage(_ sender: Any) {
    checkboxHandler()
  }
  
  private func setTextLblTapGestureRecognizer() {
    guard textLbl.gestureRecognizers?.count != 1 else {
      return
    }
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.textLblTapHandler))
    textLbl.addGestureRecognizer(tap)
  }
  
  @objc private func textLblTapHandler() {
    addTextHandler()
  }
}
