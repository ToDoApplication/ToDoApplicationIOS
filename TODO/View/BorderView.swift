//
//  BorderView.swift
//  TODO
//
//  Created by Marcin Jucha on 09.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class BorderView: UIView {

  override func awakeFromNib() {
    super.awakeFromNib()
    
    layer.borderWidth = 1
    layer.borderColor = UIColor(red: 101/255, green: 128/255, blue: 142/255, alpha: 1).cgColor
    layer.cornerRadius = 10.0
    clipsToBounds = true
  }
}
