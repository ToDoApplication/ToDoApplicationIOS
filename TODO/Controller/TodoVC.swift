//
//  ViewController.swift
//  TODO
//
//  Created by Marcin Jucha on 08.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class TodoVC: UIViewController, TodoVCDelegte {

  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var addBtn: UIBarButtonItem!
  
  var todoGroup: TodoGroup? {
    get {
      return _todoGroup
    }
    set {
      _todoGroup = newValue
    }
  }
  
  var manager = todoGroupManager
  
  fileprivate var _todoGroup: TodoGroup?
  
  fileprivate var _todoList: [Todo] {
    if let group = _todoGroup {
      return group.list
    }
    return []
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    addBtn.isEnabled = false
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadTodoGroup()
    
    setMenuProperties()
    
    tableView.setProperties(self)
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.menuNotificationReceived(notification:)),
                                           name: NSNotification.Name.MENU_NOTIFICATION, object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tableView.reloadData()
  }
  
  func reloadData() {
    tableView.reloadData()
  }

  private func loadTodoGroup() {
    setNetworkManagerDelegate(self)
    
    manager.loadTodoGroupList {
      self.addBtn.isEnabled = true
      self._todoGroup = self.manager.getTodoGroupList().first
      self.refreashTodoGroup()
    }
  }
  
  @IBAction func menuBtnAction(_ sender: Any) {
    toggleLeftMenu()
  }
  
  func menuNotificationReceived(notification: Notification) {
    if let group = notification.object as? TodoGroup {
      _todoGroup = group
      refreashTodoGroup()
    }
    view.isUserInteractionEnabled = !view.isUserInteractionEnabled
  }
  
  private func refreashTodoGroup() {
    if let group = _todoGroup {
      navigationItem.title = group.title
    } else {
      navigationItem.title = ""
    }
    _todoGroup?.delegate = self
    tableView.reloadData()
  }

  @IBAction func addTodoBtnAction(_ sender: Any) {
    performSegue(withIdentifier: TODO_DETAILS_SEGUE, sender: nil)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let destination = segue.destination as? TodoDetailsVC {
      if let item = sender as? Todo {
        destination.todo = item
      }
      destination.todoGroup = _todoGroup
    }
  }
  
  @IBAction func unwindToTodoList(segue: UIStoryboardSegue) {}
}

extension TodoVC: UITableViewDataSource, UITableViewDelegate {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return _todoList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(forIndex: indexPath) as TodoCell
    cell.configureCell(_todoList[indexPath.row]) {
      [weak self] in
      self?.tableView.reloadRows(at: [indexPath], with: .none)
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let item = _todoList[indexPath.row]
    performSegue(withIdentifier: TODO_DETAILS_SEGUE, sender: item)
  }
}

