//
//  AppDelegate.swift
//  TODO
//
//  Created by Marcin Jucha on 08.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    FirebaseApp.configure()
    FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    
    return true
  }
  
  func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
    let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    
    return handled
  }
}

let todoGroupManager: TodoGroupManagerProtocol = TodoGroupManager()
var networkManager: NetworkManagerProtocol = NetworkManager()

func setNetworkManagerDelegate(_ delegate: NetworkManagerDelegate) {
  networkManager.delegate = delegate
}
