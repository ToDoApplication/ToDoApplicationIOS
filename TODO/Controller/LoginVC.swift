//
//  LoginVC.swift
//  TODO
//
//  Created by Marcin Jucha on 27.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth

class LoginVC: UIViewController {

  @IBOutlet weak var spinner: UIActivityIndicatorView!
  @IBOutlet weak var buttonView: UIView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if FBSDKAccessToken.current() != nil {
      if let token = FBSDKAccessToken.current().tokenString {
        disableLoginButton()
        FBSDKGraphRequest(graphPath: "me",
                          parameters: ["fields":"email,name"],
                          tokenString: token,
                          version: nil,
                          httpMethod: "GET").start() { (connection, result, error) in
                            
            guard let result = result as? [String: Any],
              let username = result["id"] as? String else {
                print("Failed to get id")
                self.enableLoginButton()
                return
            }
                            
            self.loginUserToWS(username: username, token: token)
        }
      }
    }
  }
  
  @IBAction func loginByFB(_ sender: Any) {
    disableLoginButton()
    loginFB()
  }
  
  private func disableLoginButton() {
    spinner.startAnimating()
    buttonView.isUserInteractionEnabled = false
  }
  
  private func enableLoginButton() {
    spinner.stopAnimating()
    buttonView.isUserInteractionEnabled = true
  }
  
  @IBAction func unwindToLogin(segue: UIStoryboardSegue) {}
  
  private func loginFB() {
    let fbLoginManager = FBSDKLoginManager()
    fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
      if let error = error {
        print("Failed to login: \(error.localizedDescription)")
        self.enableLoginButton()
        return
      }
      
      guard let accessToken = FBSDKAccessToken.current() else {
        print("Failed to get access token")
        self.enableLoginButton()
        return
      }
      
      FBSDKGraphRequest(graphPath: "me",
                        parameters: ["fields":"email,name"],
                        tokenString: accessToken.tokenString,
                        version: nil,
                        httpMethod: "GET").start() { (connection, result, error) in
          
          guard let result = result as? [String: Any],
            let username = result["id"] as? String else {
            self.enableLoginButton()
            print("Failed to get id")
            return
          }
          
          let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
          
          // Perform login by calling Firebase APIs
          Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
              print("Login error: \(error.localizedDescription)")
              let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
              let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
              alertController.addAction(okayAction)
              self.present(alertController, animated: true, completion: nil)
              
              return
            }
            
            // ****************************************
            // MARK: Send Token With Email to Server
            // ****************************************
            
            // Present the main view
            self.loginUserToWS(username: username, token: accessToken.tokenString)
          }
      }
    }
  }
  
  private func loginUserToWS(username: String, token: String) {
    setNetworkManagerDelegate(self)
    networkManager.signInUser(username: username, token: token) {
      error in
      
      self.enableLoginButton()
      if error == nil {
        self.performSegue(withIdentifier: MAIN_SEGUE, sender: token)
      }
    }
  }
}
