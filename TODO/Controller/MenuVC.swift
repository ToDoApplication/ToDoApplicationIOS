//
//  LeftMenuVC.swift
//  TODO
//
//  Created by Marcin Jucha on 09.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth

class MenuVC: UIViewController {

  @IBOutlet weak var stackViewTrailingConstraints: NSLayoutConstraint!
  
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var tableView: UITableView!
  
  @IBOutlet weak var logoutBtn: UIButton!
  
  var manager = todoGroupManager
  
  fileprivate(set)var isEditingMode = false
  
  var _groupList: [TodoGroup] {
    return manager.getTodoGroupList()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    stackViewTrailingConstraints.constant = 60.0
    
    tableView.setProperties(self)
    tableView.register(TodoGroupCell.self)
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.disableEditingMode), name: NSNotification.Name.MENU_EDIT_NOTIFICATION, object: nil)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tableView.reloadData()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  func disableEditingMode() {
    isEditingMode = false
    tableView.reloadData()
  }

  @IBAction func hideMenu(_ sender: Any) {
    toggleLeftMenu()
  }
  
  @IBAction func logoutUser(_ sender: Any) {
    FBSDKLoginManager().logOut()
    try? Auth.auth().signOut()
    performSegue(withIdentifier: UNWIND_TO_LOGIN, sender: nil)
  }
  
  @IBAction func addGroup(_ sender: Any) {
    presentTodoGroupVC(delegate: self)
  }
  
  @IBAction func editGroup(_ sender: Any) {
    isEditingMode = !isEditingMode
    tableView.reloadData()
  }
}

//MARK: TodoGroupVC Delegate
extension MenuVC: TodoGroupVCDelegate {
  func makingChangesTodoGroupCompleted() {
    tableView.reloadData()
  }
}

//MARK: Table View
extension MenuVC: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return _groupList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(forIndex: indexPath) as TodoGroupCell
    let group = _groupList[indexPath.row]
    cell.configureCell(group)
    cell.isEditingMode = isEditingMode
    
    cell.deleteHandler = {
      [weak self] in
        self?.deleteRowAlert(group)
    }
    
    cell.editHandler = {
      [weak self] in
        self?.editTodoGroup(group)
    }
    
    return cell
  }
  
  private func deleteRowAlert(_ group: TodoGroup) {
    if _groupList.count > 1 {
      let alert = UIAlertController.deleteAlertWithText(title: "Do you want to delete group?") {
        [weak self] in
          self?.deleteRow(group)
      }
      present(alert, animated: true, completion: nil)
    } else {
      let alert = UIAlertController.createOneButtonAlertWithText(title: "There has to be at least one group.")
      present(alert, animated: true, completion: nil)
    }
  }
  
  func deleteRow(_ group: TodoGroup) {
    if let index = _groupList.indexOfElement(element: group) {
      manager.deleteTodoGroup(group)
      tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
    }
  }
  
  private func editTodoGroup(_ group: TodoGroup) {
    presentTodoGroupVC(delegate: self, group: group)
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let item = _groupList[indexPath.row]
    toggleLeftMenu(item)
  }
}

//MARK: UISearchBar Delegate
extension MenuVC: UISearchBarDelegate {
  
}
