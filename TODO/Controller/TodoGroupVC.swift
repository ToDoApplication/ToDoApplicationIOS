//
//  TodoGroupVC.swift
//  TODO
//
//  Created by Marcin Jucha on 21.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class TodoGroupVC: UIViewController {
  
  @IBOutlet weak var scrollView: UIScrollView!
  
  @IBOutlet weak var backgrounView: UIView!
  @IBOutlet weak var todoGroupView: TodoGroupView!
  
  @IBOutlet weak var textField: UITextField!
  @IBOutlet weak var stackViewWithColors: TodoGroupColorStackView!
  
  var delegate: TodoGroupVCDelegate?
  var manager = todoGroupManager
  
  var todoGroup: TodoGroup? {
    didSet {
      if let group = todoGroup {
        _todoGroup = group
      }
    }
  }
  
  fileprivate var _todoGroup = TodoGroup()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setOutletsData()
    
    scrollView.makeAwareOfKeyboard()
    
    let extractedExpr: () = textField.delegate = self
    extractedExpr
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
    backgrounView.addGestureRecognizer(tap)
  }
  
  private func setOutletsData() {
    textField.text = _todoGroup.title
    stackViewWithColors.selectedColor(ColorViewTag.getColorViewTag(_todoGroup.color))
  }
  
  func hideKeyboard() {
    view.endTextEditing()
  }
  
  deinit {
    // remove notification from scollView
    NotificationCenter.default.removeObserver(self)
  }
  
  @IBAction func cancelBtnHandler(_ sender: Any) {
    dismissViewController()
  }
  
  @IBAction func saveBtnHandler(_ sender: Any) {
    guard validateFields() else {
      let alert = UIAlertController.createOneButtonAlertWithText(title: "Please enter group title.")
      present(alert, animated: true, completion: nil)
      return
    }
    
    setTodoGroupData()
    setNetworkManagerDelegate(self)
    
    if let _ = todoGroup {
      manager.updateTodoGroup(_todoGroup) {
        self.delegate?.makingChangesTodoGroupCompleted()
      }
    } else {
      manager.addTodoGroup(_todoGroup) {
        self.delegate?.makingChangesTodoGroupCompleted()
      }
    }
    
    dismissViewController()
  }
  
  private func validateFields() -> Bool {
    if let text = textField.text {
      return !text.isEmpty
    }
    return false
  }
  
  private func setTodoGroupData() {
    _todoGroup.title = textField.text ?? ""
    _todoGroup.color = stackViewWithColors.getSelectedColor()
  }
}

extension TodoGroupVC: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    view.endTextEditing()
    return true
  }
}







