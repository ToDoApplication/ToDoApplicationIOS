//
//  TodoVC.swift
//  TODO
//
//  Created by Marcin Jucha on 09.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import UIKit

class TodoDetailsVC: UIViewController {
  
  @IBOutlet weak var trashBtn: UIBarButtonItem!
  
  @IBOutlet weak var titleTextField: UITextField!
  @IBOutlet weak var favoriteImgBtn: ImageButton!
  
  @IBOutlet weak var groupTitleLbl: UILabel!
  @IBOutlet weak var groupColorView: UIView!
  
  @IBOutlet weak var segmentType: UISegmentedControl!
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var textView: UITextView!
  
  var todoGroup: TodoGroup!
  var todo: Todo? {
    didSet {
      if let item = todo {
        _todo = item.copy()
      }
    }
  }
  
  var favorite = false
  
  var _todo = Todo()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    initializeOutletsWithTodoData()
    
    tableView.setProperties(self)
    tableView.register(TodoListItemCell.self)
    tableView.register(TodoListItemAddCell.self)
    
    titleTextField.delegate = self
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
    view.addGestureRecognizer(tap)
  }
  
  func hideKeyboard() {
    view.endTextEditing()
  }
  
  @IBAction func deleteTodo(_ sender: Any) {
    let alert = UIAlertController.deleteAlertWithText(title: "Do you want to delete todo?") {
        self.deleteTodo()
    }
    present(alert, animated: true, completion: nil)
  }
  
  func deleteTodo() {
    todoGroup.removeTodo(_todo)
    unwindToTodoList()
  }
  
  @IBAction func saveTodo(_ sender: Any) {
    guard validateFields() else {
      let alert = UIAlertController.createOneButtonAlertWithText(title: "Please enter task title and details.")
      present(alert, animated: true, completion: nil)
      return
    }
    setTodoData()
    
    if todo != nil {
      todoGroup.updateTodo(_todo)
    } else {
      todoGroup.addTodo(_todo)
    }
    
    unwindToTodoList()
  }
  
  private func validateFields() -> Bool {
    if let text = titleTextField.text {
      return !text.isEmpty
    }
    return false
  }
  
  private func setTodoData() {
    _todo.title = titleTextField.text ?? ""
    _todo.type = segmentType.selectedSegmentIndex == 0 ? .plain : .list
    
    if segmentType.selectedSegmentIndex == 0 {
      _todo.text = textView.text
    }
  }
  
  @IBAction func changeTodoType(_ sender: Any) {
    if segmentType.selectedSegmentIndex == 0 {
      textView.isHidden = false
      tableView.isHidden = true
    } else {
      textView.isHidden = true
      tableView.isHidden = false
    }
  }
  
  @IBAction func changeFavorite(_ sender: Any) {
    _todo.favorite = !_todo.favorite
    favoriteImgBtn.setImage(_todo.getImage(), for: .normal)
  }
  
  private func unwindToTodoList() {
    performSegue(withIdentifier: UNWIND_TO_TODO_LIST, sender: nil)
  }
}

// ****************************************
// MARK: Table View
// ****************************************
extension TodoDetailsVC: UITableViewDataSource, UITableViewDelegate {
  private var numberOfNotCompletedTasks: Int {
    return _todo.list.filter({ $0.completed == false }).count + 1
  }
  
  private var numberOfCompletedTasks: Int {
    return _todo.list.filter({ $0.completed == true }).count
  }
  
  private var listOfNotCompletedItems: [TodoListItem] {
    return _todo.list.filter() { $0.completed == false }
  }
  
  private var listOfCompletedItems: [TodoListItem] {
    return _todo.list.filter() { $0.completed == true }
  }
  
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if numberOfCompletedTasks == 0 {
      return 1
    } else {
      return 2
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return numberOfNotCompletedTasks
    } else {
      return numberOfCompletedTasks
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
      return cellForZeroSection(tableView, forIndex: indexPath)
    } else {
      return todoListItemCell(tableView, forIndex: indexPath)
    }
  }
  
  private func cellForZeroSection(_ tableView: UITableView, forIndex indexPath: IndexPath) -> UITableViewCell {
    if numberOfNotCompletedTasks - 1 == indexPath.row {
      return todoListItemAddCell(tableView, forIndex: indexPath)
    } else {
      return todoListItemCell(tableView, forIndex: indexPath)
    }
  }
  
  private func todoListItemAddCell(_ tableView: UITableView, forIndex indexPath: IndexPath) -> TodoListItemAddCell {
    let cell = tableView.dequeueReusableCell(forIndex: indexPath) as TodoListItemAddCell
    cell.configureCell {
      [weak self] in
        self?.insertTodoListItem()
    }
    
    return cell
  }
  
  private func insertTodoListItem() {
    _todo.addTodoListItem(TodoListItem())
    let count = numberOfNotCompletedTasks - 2
    tableView.insertRows(at: [IndexPath(row: count, section: 0)], with: .fade)
  }
  
  private func todoListItemCell(_ tableView: UITableView, forIndex indexPath: IndexPath) -> TodoListItemCell {
    let cell = tableView.dequeueReusableCell(forIndex: indexPath) as TodoListItemCell
    
    let item = getTodoItemList(indexPath)
    cell.configureCell(item) {
      [weak self] in
        self?.presentTextViewController(item)
    }
    
    cell.checkboxHandler = {
      [weak self] in
        self?.listItemCheckboxHandler(item)
    }
    
    return cell
  }
  
  private func getTodoItemList(_ indexPath: IndexPath) -> TodoListItem {
    if indexPath.section == 0 {
      return listOfNotCompletedItems[indexPath.row]
    } else {
      return listOfCompletedItems[indexPath.row]
    }
  }
  
  private func listItemCheckboxHandler(_ item: TodoListItem) {
    item.completed = !item.completed
    tableView.reloadData()
  }
  
  private func presentTextViewController(_ item: TodoListItem) {
    let alertController = UIAlertController.createAlertWithTextField(title: "Enter Text", text: item.text) {
      (text) in
        self.saveActionHandler(text, item)
    }
    
    present(alertController, animated: true, completion: nil)
  }
  
  private func saveActionHandler(_ text: String, _ item: TodoListItem) {
    if let index = _todo.list.indexOfElement(element: item) {
      _todo.list[index].text = text
      let row = item.completed
                    ? listOfCompletedItems.indexOfElement(element: item)!
                    : listOfNotCompletedItems.indexOfElement(element: item)!
      
      tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
    }
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    let alert = UIAlertController.deleteAlertWithText(title: "Do you want to delete list item?") { 
      self.deleteTodoListItem(indexPath)
    }
    present(alert, animated: true, completion: nil)
  }
  
  func deleteTodoListItem(_ indexPath: IndexPath) {
    let item = indexPath.section == 0
                  ? listOfNotCompletedItems[indexPath.row]
                  : listOfCompletedItems[indexPath.row]
    _todo.removeTodoListItem(item)
    
    if indexPath.section == 1 && listOfCompletedItems.count == 0 {
      tableView.reloadData()
    } else {
      tableView.deleteRows(at: [indexPath], with: .fade)
    }
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    if section == 0 {
      return "Tasks to do"
    } else {
      return "Completed tasks"
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 30.0
  }
  
}

extension TodoDetailsVC: UITextFieldDelegate, UITextViewDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    view.endTextEditing()
    return false
  }
}

// ****************************************
// MARK: Initialize
// ****************************************
fileprivate extension TodoDetailsVC {
  fileprivate func initializeOutletsWithTodoData() {
    if todo == nil {
      trashBtn.isEnabled = false
    }
    
    initializeOutlets(_todo)
  }
  
  fileprivate func initializeOutlets(_ item: Todo) {
    titleTextField.text = item.title
    favoriteImgBtn.setImage(item.getImage(), for: .normal)
    segmentType.selectedSegmentIndex = item.type.rawValue
    
    switch item.type {
      
      case .plain:
        textView.isHidden = false
        tableView.isHidden = true
      case .list:
        textView.isHidden = true
        tableView.isHidden = false
    }
    
    textView.text = item.text
    textView.lightGrayBorder()
  }
}


