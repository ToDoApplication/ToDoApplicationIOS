//
//  PresentViewController.swift
//  TODO
//
//  Created by Marcin Jucha on 22.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol PresentViewController {}

extension UIViewController: PresentViewController {}

extension PresentViewController where Self: UIViewController {
  
  func presentTodoGroupVC(delegate: TodoGroupVCDelegate? = nil, group: TodoGroup? = nil) {
    if let vc = UIStoryboard.createTodoGroupVC(delegate: delegate, group: group) {
      vc.view.frame.origin.y = view.frame.height
      navigationController?.addChildViewController(vc)
      navigationController?.view.addSubview(vc.view)
      
      UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseOut], animations: {
        vc.view.frame.origin.y = 0.0
      })
    }
  }
  
  func dismissViewController() {
    UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseIn], animations: {
      self.view.frame.origin.y = self.view.frame.height
    }) { flag in
      self.removeFromParentViewController()
      self.view.removeFromSuperview()
    }
  }
  
}
