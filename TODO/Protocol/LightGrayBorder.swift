//
//  LightGrayBorder.swift
//  TODO
//
//  Created by Marcin Jucha on 10.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol LightGrayBorder {}

extension UIView: LightGrayBorder {}

extension LightGrayBorder where Self: UIView {
  func lightGrayBorder() {
    layer.borderColor = UIColor.lightGray.cgColor
    layer.borderWidth = 1.0
    layer.cornerRadius = 5.0
  }
}
