//
//  NibLoadableView.swift
//  TODO
//
//  Created by Marcin Jucha on 16.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol NibLoadableView {}

extension NibLoadableView where Self: UITableViewCell {
  static var nibName: String {
    return String(describing: self)
  }
}

extension UITableViewCell: NibLoadableView {}
