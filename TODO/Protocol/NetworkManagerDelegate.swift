//
//  NetworkManagerDelegate.swift
//  TODO
//
//  Created by Marcin Jucha on 11.06.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol NetworkManagerDelegate {
  func displayAlert(_ status: HttpStatusMessage)
  func displayAlert(_ message: String)
}

extension NetworkManagerDelegate where Self: UIViewController {
  func displayAlert(_ message: String) {
    let alert = UIAlertController.createAlertWithText(title: "Network Error", message: message)
    present(alert, animated: true, completion: nil)
  }
  
  func displayAlert(_ status: HttpStatusMessage) {
    var alert: UIAlertController!
    if status == .unauthorized {
      alert = UIAlertController.createOneButtonAlertWithText(title: "Network Error", message: status.message()) {
        self.performSegue(withIdentifier: UNWIND_TO_LOGIN_LIST, sender: nil)
      }
    } else {
      alert = UIAlertController.createAlertWithText(title: "Network Error", message: status.message())
    }
    self.present(alert, animated: true, completion: nil)
  }
}

extension UIViewController: NetworkManagerDelegate {}
