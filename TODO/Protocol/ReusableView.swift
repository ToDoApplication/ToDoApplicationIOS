//
//  ReusableView.swift
//  TODO
//
//  Created by Marcin Jucha on 08.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol ReusableView {}

extension ReusableView where Self: UITableViewCell {
  static var reuseIdentifier: String {
    return String(describing: self)
  }
}

extension UITableViewCell: ReusableView {}
