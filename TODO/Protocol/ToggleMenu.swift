//
//  MenuProtocol.swift
//  TODO
//
//  Created by Marcin Jucha on 08.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol ToggleMenu {}

extension ToggleMenu where Self: UIViewController {
  
  func setMenuProperties() {
    let width: CGFloat = view.frame.width - 70.0
  
    if let rvc = revealViewController() {
      rvc.frontViewShadowOpacity = 0
      rvc.rearViewRevealWidth = width
      rvc.rightViewRevealWidth = width
    }
  }
  
  func toggleLeftMenu(_ object: Any? = nil) {
    if let rvc = revealViewController() {
      rvc.revealToggle(animated: true)
      NotificationCenter.default.post(name: NSNotification.Name.MENU_NOTIFICATION, object: object)
      NotificationCenter.default.post(name: NSNotification.Name.MENU_EDIT_NOTIFICATION, object: nil)
    }
  }
  
  func toggleRightMenu() {
    if let rvc = revealViewController() {
      rvc.rightRevealToggle(animated: false)
    }
  }
}

extension UIViewController: ToggleMenu {}
