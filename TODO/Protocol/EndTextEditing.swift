//
//  EndTextEditing.swift
//  TODO
//
//  Created by Marcin Jucha on 10.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol EndTextEditing {}

extension UIView: EndTextEditing {}

extension EndTextEditing where Self: UIView {
  func endTextEditing() {
    self.endEditing(false)
  }
}
