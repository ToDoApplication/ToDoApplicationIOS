//
//  TodoGroupVCDelegate.swift
//  TODO
//
//  Created by Marcin Jucha on 22.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol TodoGroupVCDelegate {
  func makingChangesTodoGroupCompleted()
}
