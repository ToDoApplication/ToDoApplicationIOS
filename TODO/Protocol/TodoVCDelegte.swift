//
//  TodoVCDelegte.swift
//  TODO
//
//  Created by Marcin Jucha on 08.06.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

import Foundation

protocol TodoVCDelegte {
  func reloadData()
}
