//
//  TodoCellTests.swift
//  TODO
//
//  Created by Marcin Jucha on 17.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import XCTest

class TodoListItemCellTests: XCTestCase {
  
  var sut: TodoListItemCell!
  var checkboxBtn: ImageButton = ImageButton()
  var textLbl = UILabel()
  
  override func setUp() {
    super.setUp()
    
    sut = TodoListItemCell()
    sut.checkboxBtn = checkboxBtn
    sut.textLbl = textLbl
  }
  
  override func tearDown() {
    sut = nil
    super.tearDown()
  }
  
  fileprivate var item: TodoListItem {
    let item = TodoListItem.examplesData().first!
    return item
  }
  
  func test_ConfigureCell_ShouldSetCheckboxBtnImageAsNotCheck_AndSetTextLblText() {
    //given 
    let data = item
    
    //when
    sut.configureCell(data) {}
    
    //then
    XCTAssertEqual(sut.checkboxBtn.imageView?.image, data.getImage())
    XCTAssertEqual(sut.textLbl.text, data.text)
  }
  
  func test_ConfigureCell_ShouldSetTapGestureRecognizer() {
  //when
    sut.configureCell(item) { }
    
  //then
    XCTAssertEqual(sut.textLbl.gestureRecognizers?.count, 1)
  }
  
  func test_ConfigureCell_ShouldSetTapGestureRecognizerOnlyOnce() {
    //when
    sut.configureCell(item) {}
    sut.configureCell(item) {}
    
    //then
    XCTAssertEqual(sut.textLbl.gestureRecognizers?.count, 1)
  }
  
  func test_ConfigureCell_ShouldSetHandler() {
  //given
   let excp = expectation(description: "handler")
    
  //when
    sut.configureCell(item) { 
      excp.fulfill()
    }
    sut.addTextHandler()
    
  //then
    waitForExpectations(timeout: 0.5) { (error) in
      if let _ = error {
        XCTFail()
      }
      XCTAssertTrue(true)
    }
  }
  
  func test_ChangeCheckboxImage_ShouldCallCheckboxHandler() {
  //given
    let exp = expectation(description: "checkbox")
    
  //when
    sut.checkboxHandler = {
      exp.fulfill()
    }
    sut.changeCheckboxImage(sut)
    
  //then
    waitForExpectations(timeout: 0.5) { (error) in
      if let _ = error {
        XCTFail()
      }
      XCTAssertTrue(true)
    }
  }
  
}
