//
//  TodoGroupColorViewTests.swift
//  TODO
//
//  Created by Marcin Jucha on 22.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import XCTest

class TodoGroupColorViewTests: XCTestCase {
  
  var sut: TodoGroupColorView!
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
    sut = TodoGroupColorView()
  }
  
  override func tearDown() {
    sut = nil
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func test_GetColor_ShouldReturnColorViewTag_FromViewTag() {
    //given 
    sut.tag = ColorViewTag.pink.rawValue
    
  //when
    let color = sut.getColor()
    
  //then
    XCTAssertEqual(color, .pink)
  }
  
  func test_AwakeFromNib_ShouldSetTapGestureRecognizer() {
  //when
    sut.awakeFromNib()
    
  //then
    XCTAssertEqual(sut.gestureRecognizers?.count, 1)
  }
  
  fileprivate class SuperviewMock: TodoGroupColorStackView {
  
    override func selectedColor(_ color: ColorViewTag) {
      self.color = color
    }
    
    var color: ColorViewTag?
    
    @discardableResult
    static func mock(_ sut: TodoGroupColorView) -> SuperviewMock {
      let mock = SuperviewMock()
      mock.addSubview(sut)
      return mock
    }
  }

  func test_SelectColor_ShouldCallSuperviewSelectedColorTag() {
  //given
    let mock = SuperviewMock.mock(sut)
    sut.tag = ColorViewTag.blue.rawValue
    
  //when
    sut.selectColor()
    
  //then
    XCTAssertEqual(mock.color, .blue)
  }
  
}
