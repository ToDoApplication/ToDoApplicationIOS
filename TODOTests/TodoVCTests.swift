//
//  TodoVCTests.swift
//  TODO
//
//  Created by Marcin Jucha on 09.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import XCTest

class TodoVCTests: XCTestCase {
  
  var sut: TodoVC!
  
  override func setUp() {
    super.setUp()
    
    sut = UIStoryboard.todoVC()
    sut.manager = TodoManagerMock()
  }
  
  override func tearDown() {
    sut = nil
    
    super.tearDown()
  }
  
  private func loadView() {
    let _ = sut.view
  }
  
  func test_ViewDidLoad_ShouldSetTableViewDataSourceDelegateRegisterCell() {
  //when
    loadView()
    
  //then
    XCTAssertNotNil(sut.tableView.dataSource)
    XCTAssertNotNil(sut.tableView.delegate)
    XCTAssertEqual(sut.tableView.estimatedRowHeight, 70)
    XCTAssertEqual(sut.tableView.rowHeight, UITableViewAutomaticDimension)
  }
  
  func test_ViewDidLoad_ShouldCallManagerLoadTodoGroupList_WithCompletionHandler() {
    //given
    loadView()
    let mock = ManagerMock.mock(sut)
    let group = mock.getTodoGroupList().first
    
    let tableMock = TableViewMock.mock(sut)
    
    //when
    sut.viewDidLoad()
    
    //then
    XCTAssertEqual(sut.todoGroup, group)
    XCTAssertEqual(tableMock.reloadDataGotCall, 1)
    XCTAssertEqual(sut.todoGroup?.delegate as? UIViewController, sut)
  }
  
  func test_ViewDidLoad_ShouldSetNavigationItemTitle_WithTodoGroupTitle_AndReloadTableViewData() {
  //given
    loadView()
    let group = sut.manager.getTodoGroupList().first!
    let mock = TableViewMock.mock(sut)
    
  //when
    sut.viewDidLoad()
    
  //then
    XCTAssertEqual(sut.navigationItem.title, group.title)
    XCTAssertEqual(mock.reloadDataGotCall, 1)
  }
  
  func test_ViewWillAppear_ShouldCallTableViewReloadData() {
  //given
    let tableMock = TableViewMock.mock(sut)
    
  //when
    sut.viewWillAppear(false)
    
  //then
    XCTAssertEqual(tableMock.reloadDataGotCall, 1)
  }
  
  func test_MenuNotificationReceived_ShouldSetViewUserInteractionOnFalse_WhenIsTrue() {
    loadView()
    
  //when
    let flag = sut.view.isUserInteractionEnabled
    sut.menuNotificationReceived(notification: Notification(name: Notification.Name.MENU_NOTIFICATION))
    
  //then
    XCTAssertEqual(sut.view.isUserInteractionEnabled, !flag)
  }
  
  func test_MenuNotificationReceived_ShouldSetNavigationItemTitle_AndReloadTableViewData_WithTodoGroupFromNotification() {
    //given
    loadView()
    var notification = Notification(name: Notification.Name.MENU_NOTIFICATION)
    let group = getTmpTodoGroup()
    group.title = "test"
    
    notification.object = group
    
    let mock = TableViewMock.mock(sut)
    
    //when
    sut.menuNotificationReceived(notification: notification)
    
    //then
    XCTAssertEqual(sut.navigationItem.title, group.title)
    XCTAssertEqual(mock.reloadDataGotCall, 1)
  }
  
  func test_AddTodoBtnAction_ShouldPerformSegue() {
  //given
    let sutMock = SutMock.mock()
    sut = sutMock
    
  //when
    sut.addTodoBtnAction(sut)
    
  //then
    XCTAssertEqual(sutMock.segueId, TODO_DETAILS_SEGUE)
    XCTAssertNil(sutMock.segueSender)
  }

  func test_PrepareForSegue_ShouldSetTodoItem() {
    //given
    let group = getTmpTodoGroup()
    sut.todoGroup = group
    let item = group.list.first!
    let segueMock = SegueMock.mock()
    let destVC = TodoDetailsVC()
    segueMock.destinationVC = destVC
    
    //when
    sut.prepare(for: segueMock, sender: item)
    
    //then
    XCTAssertEqual(destVC.todo, item)
    XCTAssertEqual(destVC.todoGroup, sut.todoGroup)
  }

}

// ****************************************
// MARK: Table View
// ****************************************
extension TodoVCTests {
  func test_NumberOfSections_Return1() {
    //then
    XCTAssertEqual(sut.numberOfSections(in: TableViewMock()), 1)
  }
  
  func test_NumberOfRowsInSection_ShouldReturnNumberOfTodoGroupList() {
    //given
    let group = getTmpTodoGroup()
    sut.todoGroup = group
    
    //when
    let count = sut.tableView(TableViewMock(), numberOfRowsInSection: 0)
    
    //then
    XCTAssertEqual(count, group.list.count)
  }
  
  func test_CellForRowAt_ShouldCallTableViewDequeueReusableCell_WithTodoCellIdentifier() {
    //given
    let tableMock = TableViewMock.mock(sut)
    sut.todoGroup = getTmpTodoGroup()
    
    //when
    _ = sut.tableView(tableMock, cellForRowAt: IndexPath(row: 0, section: 0))
    
    //then
    XCTAssertEqual(tableMock.cellId, TodoCell.reuseIdentifier)
  }
  
  func test_CellForRow_ShouldCallTodoCellConfigureCell_WithTodoAndHandler() {
    //given
    let tableMock = TableViewMock.mock(sut)
    let ip = IndexPath(row: 0, section: 0)
    let group = getTmpTodoGroup()
    sut.todoGroup = group
    
    //when
    _ = sut.tableView(tableMock, cellForRowAt: ip)
      
    //then
    let cell = tableMock.cell as! CellMock
    cell.handler?()
    
    XCTAssertEqual(cell.todo, group.list[ip.row])
    XCTAssertEqual(tableMock.indexPaths, [ip])
  }
  
  func test_DidSelectRow_ShouldCallPerformSegue_WithTodoFromRow() {
    //given
    let mockSut = SutMock.mock()
    sut = mockSut
    
    let group = getTmpTodoGroup()
    sut.todoGroup = group
    
    let tableMock = TableViewMock.mock(sut)
    
    let ip = IndexPath(row: 1, section: 0)
    
    //when
    sut.tableView(tableMock, didSelectRowAt: ip)
    
    //then
    XCTAssertEqual(mockSut.segueId, TODO_DETAILS_SEGUE)
    XCTAssertEqual((mockSut.segueSender as? Todo), group.list[ip.row])
  }
}

fileprivate class TableViewMock: UITableView {
  override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
    cellId = identifier
    return cell
  }
  
  override func reloadRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation) {
    self.indexPaths = indexPaths
  }
  
  override func reloadData() {
    reloadDataGotCall += 1
  }
  
  var cellId: String?
  var cell: TodoCell = CellMock()
  var indexPaths: [IndexPath] = []
  var reloadDataGotCall = 0
  
  @discardableResult
  static func mock(_ sut: TodoVC) -> TableViewMock {
    let mock = TableViewMock()
    sut.tableView = mock
    return mock
  }
}

fileprivate class CellMock: TodoCell {
  var todo: Todo?
  var handler: (() -> ())?
  override func configureCell(_ todo: Todo, tapImageHandler: @escaping () -> ()) {
    self.todo = todo
    handler = tapImageHandler
  }
}

fileprivate class SegueMock: UIStoryboardSegue {
  
  override var destination: UIViewController {
    return destinationVC
  }
  
  var destinationVC = UIViewController()
  
  @discardableResult
  static func mock() -> SegueMock {
    let mock = SegueMock(identifier: nil, source: UIViewController(), destination: UIViewController())
    return mock
  }
}

fileprivate class SutMock: TodoVC {
  
  override func performSegue(withIdentifier identifier: String, sender: Any?) {
    segueId = identifier
    segueSender = sender
  }
  
  var segueSender: Any?
  var segueId: String?
  
  @discardableResult
  static func mock() -> SutMock {
    let mock = SutMock()
    return mock
  }
}

fileprivate class ManagerMock: TodoManagerMock {
  
  override func loadTodoGroupList(completion: @escaping () -> ()) {
    completion()
  }
  
  @discardableResult
  static func mock(_ sut: TodoVC) -> ManagerMock {
    let mock = ManagerMock()
    sut.manager = mock
    return mock
  }
}




