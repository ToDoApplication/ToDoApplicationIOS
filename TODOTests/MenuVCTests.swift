//
//  MenuVCTests.swift
//  TODO
//
//  Created by Marcin Jucha on 19.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import XCTest

class MenuVCTests: XCTestCase {
  
  var sut: MenuVC!
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
    sut = UIStoryboard.menuVC()
    sut.manager = TodoManagerMock()
  }
    
  override func tearDown() {
    sut = nil
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  fileprivate func loadView() {
    let _ = sut.view
  }
  
  func test_ViewDidLoad_ShouldSetStackViewTrailingConstraintTo60() {
  //when
    loadView()
    
  //then
    XCTAssertEqual(sut.stackViewTrailingConstraints.constant, 60)
  }
  
  func test_ViewDidLoad_ShouldSetTableViewProperties() {
  //when
    loadView()
    
  //then
    XCTAssertEqual(sut.tableView.delegate as? UIViewController, sut)
    XCTAssertEqual(sut.tableView.dataSource as? UIViewController, sut)
    XCTAssertEqual(sut.tableView.estimatedRowHeight, 70)
    XCTAssertEqual(sut.tableView.rowHeight, UITableViewAutomaticDimension)
  }
  
  func test_ViewDidLoad_ShouldCallTableViewRegisterCell() {
  //given
    loadView()
    let mock = TableViewMock.mock(sut)
    
  //when
    sut.viewDidLoad()
    
  //then
    XCTAssertNotNil(mock.registerNib)
    XCTAssertEqual(mock.registerId, TodoGroupCell.reuseIdentifier)
  }
  
  func test_EditGroup_ShouldChangeIsEditingFlag_AndReloadTableView() {
    //given
    let flag = sut.isEditingMode
    let mock = TableViewMock.mock(sut)
    
  //when
    sut.editGroup(sut)
    
  //then
    XCTAssertEqual(sut.isEditingMode, !flag)
    XCTAssertEqual(mock.reloadDataGotCall, 1)
  }

  func test_TodoGroupVCDelegate_ShouldReloadTableViewData() {
    //given
    let mock = TableViewMock.mock(sut)
    
    //when
    sut.makingChangesTodoGroupCompleted()
    
    //then
    XCTAssertEqual(mock.reloadDataGotCall, 1)
  }
  
  func test_DisableEditingMenu_ShouldSetIsEditingFlagOnFalseAndReloadTableViewData() {
  //given
    loadView()
    sut.editGroup(sut)
    let tableMock = TableViewMock.mock(sut)
    
  //when
    sut.disableEditingMode()
    
  //then
    XCTAssertFalse(sut.isEditingMode)
    XCTAssertEqual(tableMock.reloadDataGotCall, 1)
  }
}

extension MenuVCTests {
  fileprivate class TableViewMock: UITableView {
    override func register(_ nib: UINib?, forCellReuseIdentifier identifier: String) {
      registerNib = nib
      registerId = identifier
    }
    
    override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
      dequeueId = identifier
      dequeueIP = indexPath
      return dequeueCell
    }
    
    override func deleteRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation) {
      deletedIndexPaths = indexPaths
    }
    
    override func reloadData() {
      reloadDataGotCall += 1
    }
    
    var reloadDataGotCall = 0
    
    var deletedIndexPaths: [IndexPath] = []
    
    var dequeueCell: UITableViewCell = CellMock.mock()
    var dequeueId: String?
    var dequeueIP: IndexPath?
    
    var registerNib: UINib?
    var registerId: String?
    
    @discardableResult
    static func mock(_ sut: MenuVC) -> TableViewMock {
      let mock = TableViewMock()
      sut.tableView = mock
      return mock
    }
  }
  
  fileprivate class CellMock: TodoGroupCell {
    override func configureCell(_ group: TodoGroup) {
      self.group = group
    }
    
    var group: TodoGroup?
    
    private static var _lbl = UILabel()
    private static var _stack = UIStackView()
    
    static func mock() -> CellMock {
      let mock = CellMock()
      mock.elementNumberLbl = _lbl
      mock.editingStackView = _stack
      return mock
    }
  }
  
  fileprivate class ManagerMock: TodoManagerMock {
    
    override func deleteTodoGroup(_ todoGroup: TodoGroup) {
      deletedTG = todoGroup
    }
    
    var deletedTG: TodoGroup?
    
    @discardableResult
    static func mock(_ sut: MenuVC) -> ManagerMock {
      let mock = ManagerMock()
      sut.manager = mock
      return mock
    }
  }

  
  func test_NumberOfSections_ShouldReturnOne() {
  //when
    let count = sut.numberOfSections(in: TableViewMock())
    
  //then
    XCTAssertEqual(count, 1)
  }

  func test_NumberOfRows_ShouldReturnNumberEqualNumberOfTodoGroupsFromManager() {
  //when
    let rows = sut.tableView(TableViewMock(), numberOfRowsInSection: 0)
    
  //then
    XCTAssertEqual(rows, sut.manager.getTodoGroupList().count)
  }
  
  func test_DequeueReusableCell_ShouldBeCallWithIdentifierForTodoGroupCell() {
  //given
    let mock = TableViewMock()
    let ip = IndexPath(row: 0, section: 0)
    
  //when
    _ = sut.tableView(mock, cellForRowAt: ip)
    
  //then
    XCTAssertEqual(mock.dequeueId, TodoGroupCell.reuseIdentifier)
    XCTAssertEqual(mock.dequeueIP, ip)
  }
  
  func test_CellForRow_ShouldCallTodoGroupCellConfigureCell_WithTodoGroup() {
  //given
    let cellMock = CellMock.mock()
    let tableMock = TableViewMock()
    tableMock.dequeueCell = cellMock
    let ip = IndexPath(row: 0, section: 0)
    let group = sut.manager.getTodoGroupList().first!
    
  //when
    _ = sut.tableView(tableMock, cellForRowAt: ip)
    
  //then
    XCTAssertEqual(cellMock.group, group)
  }
  
  func test_CellForRow_ShouldSetIsEditingModeFlag() {
  //given
    let tableMock = TableViewMock.mock(sut)
    sut.editGroup(tableMock)
    let ip = IndexPath(row: 0, section: 0)
    
  //when
    let cell = sut.tableView(tableMock, cellForRowAt: ip) as? CellMock
    
  //then
    XCTAssertEqual(cell?.isEditingMode, sut.isEditingMode)
  }
  
  func test_CellForRow_ShouldSetDeleteHandler() {
  //given
    let tableMock = TableViewMock.mock(sut)
    let managerMock = ManagerMock.mock(sut)
    let ip = IndexPath(row: 0, section: 0)
    let group = sut._groupList.first!
    
  //when
    sut.deleteRow(group)
    
  //then
    XCTAssertEqual(managerMock.deletedTG, group)
    XCTAssertEqual(tableMock.deletedIndexPaths, [ip])
  }
}

