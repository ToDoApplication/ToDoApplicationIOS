//
//  Helper.swift
//  TODO
//
//  Created by Marcin Jucha on 18.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import Foundation

func getTmpTodo() -> Todo {
  return Todo.tmpData()
}

func getTmpTodoWithoutNotCompletedTask() -> Todo {
  let todo = Todo(list: TodoListItem.examplesData().filter { $0.completed == true })
  return todo
}

func getTmpTodoWithoutCompletedTask() -> Todo {
  let todo = Todo(list: TodoListItem.examplesData().filter { $0.completed == false })
  return todo
}

func getTmpTodoWithOneCompletedTask() -> Todo {
  let list = [TodoListItem.examplesData().filter({ $0.completed == true }).first!]
  let todo = Todo(list: list)
  return todo
}


func getTmpTodoGroup() -> TodoGroup {
  return TodoGroup.testData()
}

class TodoManagerMock: TodoGroupManagerProtocol {
  
  fileprivate var list = TodoGroup.examplesData()
  
  func loadTodoGroupList(completion: @escaping () -> ()) {
    completion()
  }
  
  func addTodoGroup(_ todoGroup: TodoGroup, completion: @escaping () -> ()) {
    list.append(todoGroup)
    completion()
  }
  
  func updateTodoGroup(_ todoGroup: TodoGroup, completion: @escaping () -> ()) {
    completion()
  }
  
  func deleteTodoGroup(_ todoGroup: TodoGroup) {
    if let index = list.indexOfElement(element: todoGroup) {
      list.remove(at: index)
    }
  }
  
  func getTodoGroupList() -> [TodoGroup] {
    return list
  }
}
