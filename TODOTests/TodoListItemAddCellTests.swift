//
//  TodoAddCellTests.swift
//  TODO
//
//  Created by Marcin Jucha on 17.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import XCTest

class TodoListItemAddCellTests: XCTestCase {
  
  var sut: TodoListItemAddCell!
  var view = UIStackView()
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
    sut = TodoListItemAddCell()
    sut.stackView = view
  }
  
  override func tearDown() {
    sut = nil
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func test_ConfigureCell_ShouldSetUITapGestureRecognizerForStackView_WithHandler() {
  //given
    let exp = expectation(description: "tapHandler")
    
  //when
    sut.configureCell {
      exp.fulfill()
    }
    sut.insertHandler?()
    
  //then
    waitForExpectations(timeout: 1.0) { (error) in
      if let _ = error {
        XCTFail()
      }
      XCTAssertTrue(true)
    }
    XCTAssertEqual(sut.stackView.gestureRecognizers?.count, 1)
  }
  
  func test_ConfigureCell_ShouldSetOnlyOneUITapGestureRecognizerForStackView() {
    //given
    //when
    sut.configureCell {}
    sut.configureCell {}
    
    //then
    XCTAssertEqual(sut.stackView.gestureRecognizers?.count, 1)
  }

  
}
