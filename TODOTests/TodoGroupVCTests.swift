
//
//  TodoGroupVCTests.swift
//  TODO
//
//  Created by Marcin Jucha on 22.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import XCTest

class TodoGroupVCTests: XCTestCase {
  
  var sut: TodoGroupVC!
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
    sut = UIStoryboard.createTodoGroupVC()
    sut.manager = TodoManagerMock()
  }
  
  override func tearDown() {
    sut = nil
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  fileprivate func loadView() {
    let _ = sut.view
  }
  
  func test_ViewDidLoad_ShouldSetTextFieldDelegate() {
    //when
    loadView()
    
    //then
    XCTAssertEqual(sut.textField.delegate as? UIViewController, sut)
  }
  
  func test_ViewDidLoad_ShouldSetBackgroundTapGestureRecognizer() {
  //when
    loadView()
    
  //then
    XCTAssertEqual(sut.backgrounView.gestureRecognizers?.count, 1)
  }
  
  func test_ViewDidLoad_ShouldSetOutletsDataFromTodoGroup_WhenTodoGroupIsNotSet() {
  //given
    loadView()
    let group = TodoGroup()
    let mock = StackViewMock.mock(sut)
    
  //when
    sut.viewDidLoad()
    
  //then
    XCTAssertEqual(sut.textField.text, group.title)
    XCTAssertEqual(mock.color, ColorViewTag.getColorViewTag(group.color))
  }
  
  func test_ViewDidLoad_ShouldSetOutletsDataFromTodoGroup_WhenTodoGroupIsSet() {
    //given
    loadView()
    let group = getTmpTodoGroup()
    sut.todoGroup = group
    let mock = StackViewMock.mock(sut)
    
    //when
    sut.viewDidLoad()
    
    //then
    XCTAssertEqual(sut.textField.text, group.title)
    XCTAssertEqual(mock.color, ColorViewTag.getColorViewTag(group.color))
  }
  
  func test_SaveBtnHandler_ShouldSetTodoGroupDataFromOutlets() {
  //given
    loadView()
    let mock = StackViewMock.mock(sut)
    mock.color = ColorViewTag.pink
    
    let group = TodoGroup()
    sut.todoGroup = group
    
    sut.textField.text = "Test"
    
  //when
    sut.saveBtnHandler(sut)
    
  //then
    XCTAssertEqual(group.title, sut.textField.text)
    XCTAssertEqual(group.color, mock.getSelectedColor())
  }
  
//  func test_SaveBtnHandler_ShouldCallManagerUpdateTodoGroup_WhenTodoGroupIsNotNil() {
//    //given
//    loadView()
//    let mock = ManagerMock.mock(sut)
//    let group = TodoGroup.testData()
//    sut.todoGroup = group
//    
//    //when
//    sut.saveBtnHandler(sut)
//    
//    //then
//    XCTAssertNotNil(sut.todoGroup)
//    XCTAssertEqual(group, mock.todoGroup)
//    XCTAssertEqual(mock.addTodoGroupGotCall, 0)
//    XCTAssertEqual(mock.updateTodoGroupGotCall, 1)
//  }
//  
//  func test_SaveBtnHandler_ShouldCallManagerAddTodoGroup_WhenTodoGroupIsNil() {
//    //given
//    loadView()
//    let mock = ManagerMock.mock(sut)
//    
//    //when
//    sut.saveBtnHandler(sut)
//    
//    //then
//    XCTAssertNil(sut.todoGroup)
//    XCTAssertEqual(mock.addTodoGroupGotCall, 1)
//    XCTAssertEqual(mock.updateTodoGroupGotCall, 0)
//  }
//  
//  func test_SaveBtnHandler_ShouldCallDelegateMakingChangesTodoGroupCompleted() {
//  //given
//    loadView()
//    let mock = DelegateMock.mock(sut)
//    
//  //when
//    sut.saveBtnHandler(sut)
//    
//  //then
//    XCTAssertEqual(mock.makingChangesTodoGroupCompletedGotCall, 1)
//  }
}

fileprivate class DelegateMock: TodoGroupVCDelegate {
  func makingChangesTodoGroupCompleted() {
    makingChangesTodoGroupCompletedGotCall += 1
  }
  
  var makingChangesTodoGroupCompletedGotCall = 0
  
  @discardableResult
  static func mock(_ sut: TodoGroupVC) -> DelegateMock {
    let mock = DelegateMock()
    sut.delegate = mock
    return mock
  }
}

fileprivate class StackViewMock: TodoGroupColorStackView {
  override func getSelectedColor() -> GroupColor {
    return (color ?? .red).color()
  }
  
  override func selectedColor(_ color: ColorViewTag) {
    self.color = color
  }
  
  var color: ColorViewTag?
  
  @discardableResult
  static func mock(_ sut: TodoGroupVC) -> StackViewMock {
    let mock = StackViewMock()
    sut.stackViewWithColors = mock
    return mock
  }
}

fileprivate class ManagerMock: TodoManagerMock {
  var todoGroup: TodoGroup?
  var addTodoGroupGotCall = 0
  override func addTodoGroup(_ todoGroup: TodoGroup, completion: @escaping () -> ()) {
    self.todoGroup = todoGroup
    addTodoGroupGotCall += 1
    completion()
  }
  
  var updateTodoGroupGotCall = 0
  override func updateTodoGroup(_ todoGroup: TodoGroup, completion: @escaping () -> ()) {
    self.todoGroup = todoGroup
    updateTodoGroupGotCall += 1
    completion()
  }
  
  @discardableResult
  static func mock(_ sut: TodoGroupVC) -> ManagerMock {
    let mock = ManagerMock()
    sut.manager = mock
    return mock
  }
}
