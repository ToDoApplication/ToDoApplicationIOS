//
//  Extension.swift
//  TODO
//
//  Created by Marcin Jucha on 09.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import UIKit

extension UIStoryboard {

  private static var main: String {
    return "Main"
  }
  
  private static var todoList: String {
    return "TodoList"
  }
  
  private static var menu: String {
    return "Menu"
  }
  
  static func todoVC() -> TodoVC {
    let sb = UIStoryboard(name: todoList, bundle: nil)
    return sb.instantiateViewController(withIdentifier: TODO_VC_IDENTIFIER) as! TodoVC
  }
  
  static func todoDetailsVC() -> TodoDetailsVC {
    let sb = UIStoryboard(name: todoList, bundle: nil)
    return sb.instantiateViewController(withIdentifier: TODO_DETAILS_VC_IDENTIFIER) as! TodoDetailsVC
  }
  
  static func menuVC() -> MenuVC {
    let sb = UIStoryboard(name: menu, bundle: nil)
    return sb.instantiateViewController(withIdentifier: MENU_VC_IDENTIFIER) as! MenuVC
  }
}

fileprivate var nextId = 0
fileprivate var nextListItemId = 0
fileprivate var nextGroupId = 0

extension Todo {
  static func examplesData() -> [Todo] {
    return [
      Todo(favorite: false),
      Todo(favorite: true),
      Todo(favorite: false),
      Todo(favorite: true),
      Todo(favorite: false),
      Todo(favorite: true)
    ]
  }
  
  static func tmpData() -> Todo {
    let item = Todo(list: TodoListItem.examplesData())
    return item
  }
  
  convenience init(favorite: Bool) {
    self.init(list: TodoListItem.examplesData())
    
    initialize()
    
    self.favorite = favorite
  }
  
  private func initialize() {
    nextId += 1
    id = nextId
    title = "To Do \(id)"
    type = id % 2 == 0 ? .plain : .list
    text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor."
  }
  
  convenience init(_ id: Int) {
    self.init()
    
    self.id = id
  }
  
}

extension TodoListItem {
  static func examplesData() -> [TodoListItem] {
    return [
      TodoListItem(completed: false),
      TodoListItem(completed: true),
      TodoListItem(completed: false),
      TodoListItem(completed: true)
    ]
  }
  
  convenience init(completed: Bool) {
    self.init()
    
    nextListItemId += 1
    id = nextListItemId
    self.completed = completed
    text = "Task \(id)"
  }
}

extension TodoGroup {
  static func testData() -> TodoGroup {
    nextGroupId += 1
    let todo = TodoGroup(id: nextGroupId)
    todo.title = "Group 1"
    todo.color = .tealBlue
    return todo
  }
  
  static func examplesData() -> [TodoGroup] {
    var groups = [TodoGroup]()
    for _ in 0..<6 {
      nextGroupId += 1
      groups.append(TodoGroup(id: nextGroupId))
    }
    return groups
  }
  
  convenience init(id: Int) {
    self.init(list: Todo.examplesData())
    self.id = id
    title = "Todo Group \(id)"
  }
}
