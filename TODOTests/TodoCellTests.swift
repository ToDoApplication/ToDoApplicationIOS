//
//  TodoCellTests.swift
//  TODO
//
//  Created by Marcin Jucha on 09.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import XCTest

class TodoCellTests: XCTestCase {
  
  var sut: TodoCell!
  var lbl: UILabel!
  var img: UIImageView!
  var view: UIView!
  
  override func setUp() {
    super.setUp()
    sut = TodoCell()
    
    lbl = UILabel()
    sut.titleLbl = lbl
    
    img = UIImageView()
    sut.favoriteImage = img
    
    view = UIView()
    sut.favoriteView = view
  }
    
  override func tearDown() {
    sut = nil
    super.tearDown()
  }
  
  func test_ConfigureCell_ShouldSetCellOutlest_WithBlankStar_WhenfavoriteIsFalse() {
  //given
  let item = Todo.examplesData()[0]
    
  //when
    sut.configureCell(item) {}
    
  //then
    XCTAssertEqual(lbl.text, item.title)
    XCTAssertFalse(item.favorite)
    XCTAssertEqual(img.image, UIImage(named: "blank-star"))
  }
  
  func test_ConfigureCell_ShouldSetCellOutlest_WithGoldStar_WhenfavoriteIsTrue() {
    //given
    let item = Todo.examplesData()[1]
    
    //when
    sut.configureCell(item) {}
    
    //then
    XCTAssertEqual(lbl.text, item.title)
    XCTAssertTrue(item.favorite)
    XCTAssertEqual(img.image, UIImage(named: "gold-star"))
  }
  
  func test_ConfigureCell_ShouldSetFavoriteViewTapGestureRecognizer_WithCompletionHandler() {
  //given
    let item = Todo.examplesData()[1]
    let flag = item.favorite
    let exc = expectation(description: "tapHandler")
    
    //when
    sut.configureCell(item) { 
      exc.fulfill()
    }
    
    sut.imageHandler()
    
    //then
    waitForExpectations(timeout: 1.0) { error in
      if let _ = error {
        XCTFail()
      }
      XCTAssertEqual(item.favorite, !flag)
    }
  }
  
}
