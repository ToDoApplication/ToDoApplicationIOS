//
//  TodoGroupCellTests.swift
//  TODO
//
//  Created by Marcin Jucha on 21.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import XCTest

class TodoGroupCellTests: XCTestCase {
  
  var sut: TodoGroupCell!
  var circle = CircleView()
  var groupTitle = UILabel()
  var elemLbl = UILabel()
  var stack = UIStackView()
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
    sut = TodoGroupCell()
    sut.circleView = circle
    sut.groupTitleLbl = groupTitle
    sut.elementNumberLbl = elemLbl
    sut.editingStackView = stack
  }
  
  override func tearDown() {
    sut = nil
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  fileprivate var tmpGroup: TodoGroup {
    return TodoGroup.testData()
  }
  
  fileprivate class CircleMock: CircleView {
    override func setNeedsDisplay() {
      setNeedsDisplayGotCall += 1
    }
    
    var setNeedsDisplayGotCall = 0
    
    static func mock(_ sut: TodoGroupCell) -> CircleMock {
      let mock = CircleMock()
      sut.circleView = mock
      return mock
    }
  }

  func test_ConfigureCell_ShouldSetOutlets() {
  //given
    let group = tmpGroup
    let mock = CircleMock.mock(sut)
    
  //when
    mock.setNeedsDisplayGotCall = 0
    sut.configureCell(group)
    
  //then
    XCTAssertEqual(sut.circleView.circleColor, group.color.color())
    XCTAssertEqual(mock.setNeedsDisplayGotCall, 1)
    XCTAssertEqual(sut.groupTitleLbl.text, group.title)
    XCTAssertEqual(sut.elementNumberLbl.text, "\(group.list.count)")
  }
  
  func test_IsEditingMode_ShouldSetEditingStackViewIsHiddenTrue_AndElemNumberLblIsHiddenFalse_WhenIsSetToFalse() {
  //when
    sut.isEditingMode = false
    
  //then
    XCTAssertTrue(sut.editingStackView.isHidden)
    XCTAssertFalse(sut.elementNumberLbl.isHidden)
  }
  
  func test_IsEditingMode_ShouldSetEditingStackViewIsHiddenFalse_AndElemNumberLblIsHiddenTrue_WhenIsSetToTrue() {
    //when
    sut.isEditingMode = true
    
    //then
    XCTAssertFalse(sut.editingStackView.isHidden)
    XCTAssertTrue(sut.elementNumberLbl.isHidden)
  }
  
  func test_DeleteAction_ShouldCallDeleteHandler() {
  //given
    let exp = expectation(description: "delete")
    sut.deleteHandler = {
      exp.fulfill()
    }
    
  //when
    sut.deleteAction(sut)
    
  //then
    waitForExpectations(timeout: 0.5) { (error) in
      if let _ = error {
        XCTFail()
      } else {
        XCTAssertTrue(true)
      }
    }
  }
  
  func test_EditAction_ShouldCallEditHandler() {
    //given
    let exp = expectation(description: "edit")
    sut.editHandler = {
      exp.fulfill()
    }
    
    //when
    sut.editAction(sut)
    
    //then
    waitForExpectations(timeout: 0.5) { (error) in
      if let _ = error {
        XCTFail()
      } else {
        XCTAssertTrue(true)
      }
    }
  }
}





