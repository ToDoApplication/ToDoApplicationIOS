 //
//  TodoVCTests.swift
//  TODO
//
//  Created by Marcin Jucha on 10.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//

@testable import TODO
import XCTest

class TodoDetailsVCTests: XCTestCase {
  
  var sut: TodoDetailsVC!
  
  override func setUp() {
    super.setUp()
    sut = UIStoryboard.todoDetailsVC()
  }
  
  override func tearDown() {
    sut = nil
    super.tearDown()
  }
  
  fileprivate func loadView() {
    _ = sut.view
  }
  
  private func outletsTest(_ item: Todo) {
    XCTAssertTrue(sut.trashBtn.isEnabled)
    
    XCTAssertEqual(sut.titleTextField.text, item.title)
    
    let image = item.getImage()
    let btnImage = sut.favoriteImgBtn.imageView?.image
    XCTAssertEqual(btnImage, image)
    
    XCTAssertEqual(sut.segmentType
      .selectedSegmentIndex, item.type.rawValue)
    XCTAssertEqual(sut.textView.text, item.text)
    
    XCTAssertEqual(sut.textView.layer.borderWidth, 1.0)
    XCTAssertEqual(sut.textView.layer.borderColor, UIColor.lightGray.cgColor)
    XCTAssertEqual(sut.textView.layer.cornerRadius, 5.0)
  }
  
  func test_ViewDidLoad_ShouldSetTodoData_WhenIsSelectedPlainText() {
  //given
    let item = Todo.examplesData().filter { todo -> Bool in
      return todo.type == .plain
    }.first!
    sut.todo = item
    
  //when
    loadView()
    
  //then
    outletsTest(item)
    
    XCTAssertTrue(sut.tableView.isHidden)
    XCTAssertFalse(sut.textView.isHidden)
  }
  
  func test_ViewDidLoad_ShouldSetTodoData_WhenIsSelectedListText() {
    //given
    let item = Todo.examplesData().filter { (todo) -> Bool in
      return todo.type == .list
    }.first!
    sut.todo = item
    
    //when
    loadView()
    
    //then
    outletsTest(item)
    
    XCTAssertFalse(sut.tableView.isHidden)
    XCTAssertTrue(sut.textView.isHidden)
  }
  
  func test_ViewDidLoad_ShouldDisableTrashBtn_WhenTodoIsNil() {
  //given
    XCTAssertNil(sut.todo)
    
  //when
    loadView()
    
  //then
    XCTAssertFalse(sut.trashBtn.isEnabled)
  }
  
  func test_ViewDidLoad_ShouldSetTapGestureRecognizerForView() {
    //when
    loadView()
    
    //then
    XCTAssertEqual(sut.view.gestureRecognizers?.count, 1)
  }
  
  func test_TableView_ShouldHaveSetDataSourceDelegate() {
  //when
    loadView()
  //then
    XCTAssertNotNil(sut.tableView.dataSource)
    XCTAssertNotNil(sut.tableView.delegate)
  }
  
  
  func test_TableView_ShouldRegisterTodoCellAndTodoAddCell() {
  //given
    loadView()
    let mock = TableViewMock.mock(sut)
    
  //when
    sut.viewDidLoad()
    
  //then
    XCTAssertTrue(mock.registerIds.contains(TodoListItemCell.reuseIdentifier))
    XCTAssertTrue(mock.registerIds.contains(TodoListItemAddCell.reuseIdentifier))
  }
  
  func test_TitleTextField_ShouldHaveSetDelegate() {
  //when
    loadView()
  
  //then
    XCTAssertNotNil(sut.titleTextField.delegate)
  }
}


fileprivate class TodoGroupMock: TodoGroup {
  
  override func removeTodo(_ todo: Todo) {
    removedTodo = todo
  }
  
  override func addTodo(_ todo: Todo) {
    addedTodo = todo
  }
  
  override func updateTodo(_ todo: Todo) {
    updatedTodo = todo
  }

  var updatedTodo: Todo?
  var addedTodo: Todo?
  var removedTodo: Todo?
  
  @discardableResult
  static func mock(_ sut: TodoDetailsVC) -> TodoGroupMock {
    let mock = TodoGroupMock()
    sut.todoGroup = mock
    return mock
  }
}


// ****************************************
// MARK: IBOutlets Actions
// ****************************************
extension TodoDetailsVCTests {
  func test_DeleteTodo_ShouldCallTodoGroupRemoveTodo_WithTodo() {
    //given
    let sutMock = SutMock.mock()
    sut = sutMock
    let todoMock = TodoGroupMock.mock(sut)
    
    let todo = getTmpTodo()
    sut.todo = todo
    
    //when
    sut.deleteTodo()
    
    //then
    XCTAssertEqual(todoMock.removedTodo, sut.todo)
    XCTAssertEqual(sutMock.segueId, UNWIND_TO_TODO_LIST)
  }
  
//  func test_SaveTodo_ShouldCallTodoGroupAddTodo_WhenTodoIsNil() {
//    //given
//    loadView()
//    let mock = TodoGroupMock.mock(sut)
//    sut.todo = nil
//    
//    //when
//    sut.saveTodo(sut)
//    
//    //then
//    XCTAssertNotNil(mock.addedTodo)
//  }
//  
//  func test_SaveTodo_ShouldCallTodoGroupUpdateTodo_WhenTodoIsNotNil() {
//    //given
//    loadView()
//    let mock = TodoGroupMock.mock(sut)
//    let item = Todo()
//    sut.todo = item
//    
//    //when
//    sut.saveTodo(sut)
//    
//    //then
//    XCTAssertEqual(mock.updatedTodo, sut.todo)
//  }
  
  func test_SaveTodo_ShouldSetTodoData() {
    //given
    loadView()
    let mock = TodoGroupMock.mock(sut); _ = mock
    sut.todo = getTmpTodo()
    
    sut.titleTextField.text = "Hello!"
    sut.segmentType.selectedSegmentIndex = 0
    sut.textView.text = "Some text"
    
    //when
    sut.saveTodo(sut)
    
    //then
    XCTAssertEqual(sut._todo.title, sut.titleTextField.text)
    XCTAssertEqual(sut._todo.type, .plain)
    XCTAssertEqual(sut._todo.text, sut.textView.text)
  }
  
  func test_SaveTodo_ShouldSetTodoData_Second() {
    //given
    loadView()
    let mock = TodoGroupMock.mock(sut); _ = mock
    sut.todo = getTmpTodo()
    
    sut.titleTextField.text = "Hello!"
    sut.segmentType.selectedSegmentIndex = 1
    sut.textView.text = "Some text"
    
    //when
    sut.saveTodo(sut)
    
    //then
    XCTAssertEqual(sut._todo.title, sut.titleTextField.text)
    XCTAssertEqual(sut._todo.type, .list)
    XCTAssertNotEqual(sut._todo.text, sut.textView.text)
  }

  func test_ChangeTodoType_ShouldHideTableViewAndShowTextView_WhenSegmentIndexIsZero() {
    //given
    loadView()
    sut.segmentType.selectedSegmentIndex = 0
    
    //when
    sut.changeTodoType(sut)
    
    //then
    XCTAssertTrue(sut.tableView.isHidden)
    XCTAssertFalse(sut.textView.isHidden)
  }
  
  func test_ChangeTodoType_ShouldShowTableViewAndHideTextView_WhenSegmentIndexIsOne() {
    //given
    loadView()
    sut.segmentType.selectedSegmentIndex = 1
    
    //when
    sut.changeTodoType(sut)
    
    //then
    XCTAssertFalse(sut.tableView.isHidden)
    XCTAssertTrue(sut.textView.isHidden)
  }
  
  func test_ChangeFavorite_ShouldSetFavoriteToTrue_WhenWasFalse() {
    //given
    sut.todo = getTmpTodo()
    let flag = sut.todo!.favorite
    loadView()
    
    //when
    sut.changeFavorite(sut)
    
    //then
    XCTAssertEqual(sut._todo.favorite, !flag)
    let image = !flag ? UIImage(named: "gold-star") : UIImage(named: "blank-stat")
    XCTAssertEqual(sut.favoriteImgBtn.imageView?.image, image)
  }
}


// ****************************************
// MARK: Table View
// ****************************************
extension TodoDetailsVCTests {
  
  private func numberOfNotCompletedTasks(_ list: [TodoListItem]) -> Int {
    return list.filter({ $0.completed == false }).count
  }
  
  private func numberOfCompletedTasks(_ list: [TodoListItem]) -> Int {
    return list.filter({ $0.completed == true }).count
  }
  
  func test_NumberOfSection_ShouldReturnOne_WhenTodoListItemDoNotHaveCompletedTasks() {
  //given
    sut.todo = getTmpTodoWithoutCompletedTask()
    XCTAssertEqual(numberOfCompletedTasks(sut.todo!.list), 0)
    
  //when
    let sections = sut.numberOfSections(in: TableViewMock.mock(sut))
    
  //then
    XCTAssertEqual(sections, 1)
  }
  
  func test_NumberOfSection_ShouldReturnTwo_WhenTodoListItemHaveCompletedTasks() {
    //given
    sut.todo = getTmpTodo()
    
    //when
    let sections = sut.numberOfSections(in: TableViewMock.mock(sut))
    
    //then
    XCTAssertEqual(sections, 2)
  }
  
  
  func test_NuberOfRows_ShouldReturnNumberOfNotCompletedTasksPlusOne_ForZeroSection() {
    //given
    let item = Todo.tmpData()
    sut.todo = item
    let number = numberOfNotCompletedTasks(item.list) + 1
    
    
    //when
    let count = sut.tableView(UITableView(), numberOfRowsInSection: 0)
    
    //then
    XCTAssertEqual(count, number)
  }
  
  func test_NuberOfRows_ShouldReturnNumberOfCompletedTasks_ForFirstSection() {
    //given
    let item = Todo.tmpData()
    sut.todo = item
    let number = numberOfCompletedTasks(item.list)
    
    
    //when
    let count = sut.tableView(UITableView(), numberOfRowsInSection: 1)
    
    //then
    XCTAssertEqual(count, number)
  }

  func test_CellForRow_ForZeroSection_ShouldCallTableViewDequeueResuableCell_WithIdentifierTodoListItemCell() {
    //given
    let mock = TableViewMock.mock(sut)
    let item = getTmpTodo()
    sut.todo = item
    let ip = IndexPath(row: 0, section: 0)
    XCTAssertGreaterThan(numberOfNotCompletedTasks(item.list), 0)
    
    //when
    _ = sut.tableView(mock, cellForRowAt: ip)
    
    //then
    XCTAssertEqual(ip, mock.dequeueIndexPath)
    XCTAssertEqual(TodoListItemCell.reuseIdentifier, mock.dequeueId)
  }
 
  func test_CellForRow_ForZeroSection_ShouldCallTableViewDequeueResuableCell_WithIdentifierTodoListItemAddCell_WhenRowIsEqualLastNotCompletedItem() {
    //given
    let mock = TableViewMock.mock(sut)
    mock.dequeueCell = ListAddCellMock.mock()
    sut.todo = getTmpTodo()
    let ip = IndexPath(row: numberOfNotCompletedTasks(sut.todo!.list), section: 0)
    
    //when
    _ = sut.tableView(mock, cellForRowAt: ip)
    
    //then
    XCTAssertEqual(ip, mock.dequeueIndexPath)
    XCTAssertEqual(TodoListItemAddCell.reuseIdentifier, mock.dequeueId)
  }
  
  func test_CellForRow_ForFirstSection_ShouldCallTableViewDequeueResuableCell_WithIdentifierTodoListItemCell() {
    //given
    let mock = TableViewMock.mock(sut)
    let item = getTmpTodo()
    sut.todo = item
    let ip = IndexPath(row: 0, section: 1)
    XCTAssertGreaterThan(numberOfCompletedTasks(item.list), 0)
    
    //when
    _ = sut.tableView(mock, cellForRowAt: ip)
    
    //then
    XCTAssertEqual(ip, mock.dequeueIndexPath)
    XCTAssertEqual(TodoListItemCell.reuseIdentifier, mock.dequeueId)
  }
  
  func test_CellForRow_ForFirstSection_ShouldCallTableViewDequeueResuableCell_WithIdentifierTodoListItemCell_WithCompletedTasks() {
    //given
    let mock = TableViewMock.mock(sut)
    let item = getTmpTodoWithoutNotCompletedTask()
    sut.todo = item
    let ip = IndexPath(row: 0, section: 1)
    XCTAssertGreaterThan(numberOfCompletedTasks(item.list), 0)
    
    //when
    _ = sut.tableView(mock, cellForRowAt: ip)
    
    //then
    XCTAssertEqual(ip, mock.dequeueIndexPath)
    XCTAssertEqual(TodoListItemCell.reuseIdentifier, mock.dequeueId)
  }
  
  func test_TodoListItemAddCell_ForZeroSection_ConfigureCell_WithAddNewRowClouser() {
    //given
    let item = Todo.tmpData()
    let listCount = numberOfNotCompletedTasks(item.list)
    sut.todo = item
    
    let mockTable = TableViewMock.mock(sut)
    let ip = IndexPath(row: listCount, section: 0)
    
    mockTable.dequeueCell = ListAddCellMock.mock()
    
    //when
    let cell = sut.tableView(mockTable, cellForRowAt: ip) as? ListAddCellMock
    cell?.handler?()
    cell?.handler?()
    
    //then
    XCTAssertEqual(mockTable.insertedRows.first!.row, numberOfNotCompletedTasks(sut._todo.list) - 1)
    XCTAssertEqual(listCount + 2, numberOfNotCompletedTasks(sut._todo.list))
  }
  
  private func listOfNotCompletedItem(_ list: [TodoListItem]) -> [TodoListItem] {
    return list.filter() { $0.completed == false }
  }
  
  private func listOfCompletedItem(_ list: [TodoListItem]) -> [TodoListItem] {
    return list.filter() { $0.completed == true }
  }
  
  func test_TodoListItemCell_ForZeroSection_ConfigureCell_TodoListItem() {
    //given
    let item = Todo.tmpData()
    sut.todo = item
    
    let tableMock = TableViewMock.mock(sut)
    let ip = IndexPath(row: 0, section: 0)
    
    let cellMock = ListCellMock.mock()
    tableMock.dequeueCell = cellMock
    
    //when
    _ = sut.tableView(tableMock, cellForRowAt: ip)
    
    //then
    XCTAssertEqual(cellMock.item, listOfNotCompletedItem(item.list)[ip.row])
  }
  
  func test_TodoListItemCell_ForOneSection_ConfigureCell_TodoListItem() {
    //given
    let item = Todo.tmpData()
    sut.todo = item
    
    let tableMock = TableViewMock.mock(sut)
    let ip = IndexPath(row: 0, section: 1)
    
    let cellMock = ListCellMock.mock()
    tableMock.dequeueCell = cellMock
    
    //when
    _ = sut.tableView(tableMock, cellForRowAt: ip)
    
    //then
    XCTAssertEqual(cellMock.item, listOfCompletedItem(item.list)[ip.row])
  }
  
  func test_TodoListItemCell_ForZeroSection_ConfigureCell_WithAddTextHandler() {
    //given
    let sutMock = SutMock.mock()
    sut = sutMock
    sut.todo = Todo.tmpData()
    
    let tableMock = TableViewMock.mock(sut)
    let ip = IndexPath(row: 0, section: 0)
    
    let cellMock = ListCellMock.mock()
    tableMock.dequeueCell = cellMock
    
    //when
    _ = sut.tableView(tableMock, cellForRowAt: ip)
    cellMock.handler?()
    
    //then
    XCTAssertTrue(sutMock.presentedVC is UIAlertController)
    if let controller = sutMock.presentedVC as? UIAlertController {
      XCTAssertEqual(controller.actions.count, 2)
      XCTAssertEqual(controller.textFields?.count, 1)
    }
  }
  
  func test_TodoListItemCell_ForZeroSection_CheckboxHandler_WithReloadDataAndChangeCompeltedFalg() {
  //given
    sut.todo = Todo.tmpData()
    let item  = sut._todo.list.first!
    let flag = item.completed
    
    let tableMock = TableViewMock.mock(sut)
    let ip = IndexPath(row: 0, section: 0)
    
    let cellMock = ListCellMock.mock()
    tableMock.dequeueCell = cellMock
    
  //when
    _ = sut.tableView(tableMock, cellForRowAt: ip)
    cellMock.checkboxHandler()
    
  //then
    XCTAssertEqual(item.completed, !flag)
    XCTAssertEqual(tableMock.reloadDataGotCall, 1)
  }

  func test_DeleteRow_ForZeroSection_ShouldDeleteItemFromListAndDeleteRow() {
    //given
    let mock = TableViewMock.mock(sut)
    
    sut.todo = getTmpTodo()
    let list = listOfNotCompletedItem(sut._todo.list)
    let item = list[list.count - 1]
    
    let ip = IndexPath(row: list.count - 1, section: 0)
    
    
    //when
    sut.deleteTodoListItem(ip)
    
    //then
    XCTAssertFalse(sut._todo.list.contains(item))
    XCTAssertEqual(mock.deletedRows, [ip])
    XCTAssertEqual(mock.reloadDataGotCall, 0)
  }
  
  func test_DeleteRow_ForOneSection_ShouldDeleteItemFromListAndDeleteRow() {
    //given
    let mock = TableViewMock.mock(sut)
    
    sut.todo = getTmpTodo()
    let list = listOfCompletedItem(sut._todo.list)
    let item = list[list.count - 1]
    
    let ip = IndexPath(row: list.count - 1, section: 1)
    
    
    //when
    sut.deleteTodoListItem(ip)
    
    //then
    XCTAssertFalse(sut._todo.list.contains(item))
    XCTAssertEqual(mock.deletedRows, [ip])
  }
  
  func test_DeleteRow_ForOneSection_ShouldReloadData_WhenItIsLastItem() {
  //given
    let mock = TableViewMock.mock(sut)
    sut.todo = getTmpTodoWithOneCompletedTask()
    let item = sut.todo!.list.first!
    
    let ip = IndexPath(row: 0, section: 1)
    
  //when
    sut.deleteTodoListItem(ip)
  
  //then
    XCTAssertFalse(sut._todo.list.contains(item))
    XCTAssertEqual(mock.reloadDataGotCall, 1)
    XCTAssertEqual(mock.deletedRows.count, 0)
  }
  
  func test_TitleForHeader_ShouldReturnString_ForZeroSection() {
  //when
    let title = sut.tableView(UITableView(), titleForHeaderInSection: 0)
  
  //then
    XCTAssertEqual(title, "Tasks to do")
  }
  
  func test_TitleForHeader_ShouldReturnString_ForOneSection() {
    //when
    let title = sut.tableView(UITableView(), titleForHeaderInSection: 1)
    
    //then
    XCTAssertEqual(title, "Completed tasks")
  }
}

// ****************************************
// MARK: Mocks
// ****************************************
fileprivate class TableViewMock: UITableView {
  override func register(_ nib: UINib?, forCellReuseIdentifier identifier: String) {
    registerIds.append(identifier)
  }
  
  override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
      dequeueIndexPath = indexPath
      dequeueId = identifier
    return dequeueCell
  }
  
  override func insertRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation) {
    insertedRows = indexPaths
  }
  
  override func reloadRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation) {
    reloadedRows = indexPaths
  }
  
  override func deleteRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation) {
    deletedRows = indexPaths
  }
  
  override func reloadData() {
    reloadDataGotCall += 1
  }
  
  var reloadDataGotCall = 0
  
  var deletedRows: [IndexPath] = []
  
  var reloadedRows: [IndexPath] = []
  
  var insertedRows: [IndexPath] = []
  
  var dequeueCell: UITableViewCell = ListCellMock.mock()
  var dequeueIndexPath: IndexPath?
  var dequeueId: String?
  
  var registerIds: [String] = []
  
  @discardableResult
  static func mock(_ sut: TodoDetailsVC) -> TableViewMock {
    let mock = TableViewMock()
    sut.tableView = mock
    return mock
  }
}

fileprivate class ListCellMock: TodoListItemCell {
  
  override func configureCell(_ item: TodoListItem, handler: @escaping () -> ()) {
    self.item = item
    self.handler = handler
  }
  
  var item: TodoListItem?
  var handler: (() -> ())?
  
  @discardableResult
  static func mock() -> ListCellMock {
    let mock = ListCellMock()
    return mock
  }
}

fileprivate class ListAddCellMock: TodoListItemAddCell {
  
  var handler: (() -> ())?
  
  override func configureCell(handler: @escaping () -> ()) {
    self.handler = handler
  }
  
  @discardableResult
  static func mock() -> ListAddCellMock {
    let mock = ListAddCellMock()
    return mock
  }
}

fileprivate class SutMock: TodoDetailsVC {
  
  override func performSegue(withIdentifier identifier: String, sender: Any?) {
    segueId = identifier
  }
  
  override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
    presentedVC = viewControllerToPresent
  }
  
  var presentedVC: UIViewController?
  var segueId: String?
  
  @discardableResult
  static func mock() -> SutMock {
    let mock = SutMock()
    return mock
  }
}







