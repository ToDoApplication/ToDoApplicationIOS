//
//  TodoGroupColorStackViewTests.swift
//  TODO
//
//  Created by Marcin Jucha on 22.05.2017.
//  Copyright © 2017 Marcin Jucha. All rights reserved.
//
@testable import TODO
import XCTest

class TodoGroupColorStackViewTests: XCTestCase {
  
  class StackViewMock: TodoGroupColorStackView {
    
    fileprivate class ColorMock: TodoGroupColorView {
      override func selectColor() {
        selectColorGotCall += 1
      }
      
      var selectColorGotCall = 0
      
      @discardableResult
      static func mock() -> ColorMock {
        let mock = ColorMock()
        return mock
      }
    }

    
    fileprivate var _subviews: [ColorMock] = []
    @discardableResult
    static func mock() -> StackViewMock {
      let mock = StackViewMock()
      
      mock._subviews.append(ColorMock.mock())
      mock._subviews.append(ColorMock.mock())
      
      for (i, sb) in mock._subviews.enumerated() {
        sb.tag = i + 10
        mock.addSubview(sb)
      }
      
      return mock
    }
  }

  var sut: StackViewMock!
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
    sut = StackViewMock.mock()
  }
  
  override func tearDown() {
    sut = nil
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func test_AwakeFromNib_ShouldSetFirstSubviewSelected() {
    //when
    sut.awakeFromNib()
    
    //then
    XCTAssertEqual(sut._subviews[0].selectColorGotCall, 1)
  }
  
  func test_SelectedColor_ShouldSetFirstSubviewSelectedFlagOnTrue() {
  //when
    sut.selectedColor(ColorViewTag(rawValue: sut.subviews[0].tag)!)
    
  //then
    XCTAssertTrue(sut._subviews[0].selected)
    XCTAssertFalse(sut._subviews[1].selected)
  }
  
  func test_SelectedColor_ShouldSetSecondSubviewSelectedFlagOnTrue() {
    //when
    sut.selectedColor(ColorViewTag(rawValue: sut.subviews[1].tag)!)
    
    //then
    XCTAssertTrue(sut._subviews[1].selected)
    XCTAssertFalse(sut._subviews[0].selected)
  }
  
  func test_GetSelectedColor_ShouldReturnSecondViewColor() {
    //given
    let color = ColorViewTag(rawValue: sut.subviews[1].tag)!
    sut.selectedColor(color)
    
    //when
    let selected = sut.getSelectedColor()
    
    //then
    XCTAssertEqual(selected, color.color())
  }
  
  func test_GetSelectedColor_ShouldReturnFirstViewColor() {
    //given
    let color = ColorViewTag(rawValue: sut.subviews[0].tag)!
    sut.selectedColor(color)
    
    //when
    let selected = sut.getSelectedColor()
    
    //then
    XCTAssertEqual(selected, color.color())
  }
  
  
  
}
